<?php

use Illuminate\Database\Seeder;

class CategoriesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [[
            'name' => "Banner",
            'slug' => "banner",
            'description' => "root category",
            'parent_id' => '0',
            'key' => '',], [
            'name' => "Kategori",
            'slug' => "category",
            'description' => "root category",
            'parent_id' => '0',
            'key' => ''], [
            'name' => "Genel",
            'slug' => "genel",
            'description' => "Category > Genel",
            'parent_id' => '2',
            'key' => 'genel']
        ];
        DB::table('categories')->truncate();

        DB::table('categories')->insert($data);

    }
}
