<?php

use Illuminate\Database\Seeder;
use App\Category;

class Bobdb extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 3; $i++) {
            DB::table('categories')->insert([
                'name' => str_random(8),
                'slug' => str_random(12),
                'description' =>str_random(100),
                'parent_id'=>'0',
                'key'=>'test'
            ]);
        }
    }
}
