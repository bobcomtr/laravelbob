<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     * Status Columns
     *          -published
     *          -draft
     * Type Columns
     *          -banner
     *          -form
     *          -menu
     *          
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('excerpt');
            $table->longText('content');
            $table->string('status')->default('published');
            $table->string('type');
            $table->string('lang')->default('tr');
            $table->integer('rank')->default(0);
            $table->integer('parent_id')->default(0);
            $table->string('template');
            $table->dateTime('published_at');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('contents');
    }
}
