const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/assets/cms/plugins/jquery-ui/jquery-ui.min.js',
    'resources/assets/cms/plugins/boostrapv3/js/bootstrap.min.js',
    'resources/assets/cms/plugins/jquery/jquery-easy.js',
    'resources/assets/cms/plugins/classie/classie.js',
    'resources/assets/cms/plugins/jquery-unveil/jquery.unveil.min.js',
    'resources/assets/cms/plugins/modernizr.custom.js',
    //'resources/assets/cms/js/scripts.js',
    'resources/assets/cms/plugins/jquery-scrollbar/jquery.scrollbar.js',
    'resources/assets/cms/plugins/jquery-bez/jquery.bez.min.js',
    'resources/assets/cms/plugins/datatables-responsive/js/lodash.min.js',
], 'public/cms/js/bob.core.js')

    .combine([
        'resources/assets/cms/js/app.js',
        'resources/assets/cms/js/ajax.js',
        'resources/assets/cms/plugins/pages.js',
    ], 'public/cms/js/app.js')

    .combine([
        'resources/assets/cms/plugins/codemirror/codemirror.js',
        'resources/assets/cms/plugins/codemirror/xml.js',
        'resources/assets/cms/plugins/summernote/js/summernote.js',
        'resources/assets/cms/plugins/sweetalert2/sweetalert2.min.js',
        'resources/assets/cms/plugins/bootstrap-select2/select2.min.js',
        'resources/assets/cms/plugins/switchery/js/switchery.min.js',
        'resources/assets/cms/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'resources/assets/cms/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/cms/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.js',
        'resources/assets/cms/plugins/bootstrap-tag/bootstrap-tagsinput.js',
        'resources/assets/cms/plugins/moment/moment.min.js',
        'resources/assets/cms/plugins/moment/moment-with-locales.min.js',
        'resources/assets/cms/plugins/bootstrap-daterangepicker/daterangepicker.js',
        'resources/assets/cms/plugins/summernote/js/accordion.js',
        'resources/assets/cms/plugins/jquery.form.js',
        'resources/assets/cms/plugins/jquery-menu-editor/editor.js',
        'resources/assets/cms/js/form.js',
    ], 'public/cms/js/form.js')
    .combine([
        'resources/assets/cms/plugins/jquery-datatable/media/js/jquery.dataTables.min.js',
        'resources/assets/cms/plugins/jquery-datatable/media/js/dataTables.bootstrap.js',
        'resources/assets/cms/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js',
        //'resources/assets/cms/plugins/jquery-datatable/extensions/FixedColumns/js/dataTables.fixedColumns.js',
        'resources/assets/cms/plugins/datatables-responsive/js/datatables.responsive.js',
        'resources/assets/cms/plugins/jquery-datatable/dataTables.rowReorder.min.js',
        'resources/assets/cms/js/datatables.js',
    ], 'public/cms/js/datatable.js')
    .copyDirectory('resources/assets/cms/plugins/pace/pace.min.js', 'public/cms/js/pace.js')
    //.copyDirectory('resources/assets/cms/plugins/debounce.js', 'public/cms/plugins/debounce.js')
    .sass('resources/assets/cms/scss/app.scss', 'public/cms/css/app.css')
    .styles([
        'resources/assets/cms/plugins/pace/pace-theme-flash.css',
        'resources/assets/cms/plugins/boostrapv3/css/bootstrap.min.css',
        'resources/assets/cms/plugins/font-awesome/css/font-awesome.css',
        'resources/assets/cms/plugins/jquery-scrollbar/jquery.scrollbar.css',
        'resources/assets/cms/plugins/bootstrap-select2/select2.css',
        'resources/assets/cms/plugins/bootstrap-tag/bootstrap-tagsinput.css',
        'resources/assets/cms/plugins/switchery/css/switchery.min.css',
        'resources/assets/cms/plugins/bootstrap-datepicker/css/datepicker3.css',
        'resources/assets/cms/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css',
        'resources/assets/cms/plugins/summernote/css/summernote.css',
        'resources/assets/cms/plugins/codemirror/codemirror.min.css',
        'resources/assets/cms/plugins/codemirror/monokai.min.css',

        'resources/assets/cms/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
        'resources/assets/cms/plugins/uploadfile/uploadfile.css',
        'resources/assets/cms/plugins/sweetalert2/sweetalert2.min.css',
        'resources/assets/cms/css/pages-icons.css',

        'resources/assets/cms/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css',
        'resources/assets/cms/plugins/jquery-datatable/reorder.css',
        
        //'resources/assets/cms/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
        'resources/assets/cms/plugins/datatables-responsive/css/datatables.responsive.css',

    ], 'public/cms/css/bob.core.css')
    .copyDirectory('resources/assets/cms/img', 'public/cms/img/')
    .copyDirectory('resources/assets/cms/fonts', 'public/cms/fonts/')
    .copyDirectory('resources/assets/cms/ico', 'public/cms/ico/')
    .version();