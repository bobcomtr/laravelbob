var icon = 'public/site/img/pin-icon.png';
var Styles = [
    {
        "stylers": [
            {
                "hue": "#dd0d0d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": 100
            },
            {
                "visibility": "simplified"
            }
        ]
    }
];

var locations = [
    ['Canatar Sigorta', 41.0393954, 28.9941087, 2, 'Haznedar Güven Mh. Menderes Cd. No:24 Kat:2 Daire:4 34160 Güngören/İSTANBUL'],
];

MapInit = function() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(41.0393954, 28.9941087),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: Styles,
        disableDefaultUI: true
    });


    // A new Info Window is created and set content
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: icon,
        });


        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {

                var content = '<div id="iw-container">' +
                    '<div class="iw-title">' + locations[i][0] + '</div>' +
                    '<div class="iw-content">' +
                    '<div class="iw-subTitle">Adres</div>' +
                    '<p>' + locations[i][4] + '</p>' +

                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                    '</div>';



                infowindow.setContent(content);
                infowindow.open(map, marker);
            }
        })(marker, i));

        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });

        google.maps.event.addListener(infowindow, 'domready', function() {

            var iwOuter = $('.gm-style-iw');
            var iwBackground = iwOuter.prev();

            // Removes background shadow DIV
            iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

            // Removes white background DIV
            iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

            // Moves the infowindow 115px to the right.
            iwOuter.parent().parent().css({ left: '115px' });

            // Moves the shadow of the arrow 76px to the left margin.
            iwBackground.children(':nth-child(1)').attr('style', function(i, s) { return s + 'left: 76px !important;' });

            // Moves the arrow 76px to the left margin.
            iwBackground.children(':nth-child(3)').attr('style', function(i, s) { return s + 'left: 76px !important;' });

            // Changes the desired tail shadow color.
            iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': '#c6755f 0px 1px 6px', 'z-index': '1' });

            // Reference to the div that groups the close button elements.
            var iwCloseBtn = iwOuter.next();

            // Apply the desired effect to the close button
            iwCloseBtn.css({ opacity: '1', right: '60px', top: '25px' });

            // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
            if ($('.iw-content').height() < 140) {
                $('.iw-bottom-gradient').css({ display: 'none' });
            }

            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
            iwCloseBtn.mouseout(function() {
                $(this).css({ opacity: '1' });
            });
        });
    }
}

// $(window).ready(function(){
//     MapInit();
// })



$(function() {
if ($("body").find(".map").length>0) {
    //if (typeof google !== "undefined"){
    if (window.google && google.maps) {
        initializeMap();
    } else {

        lazyLoadGoogleMap();
    }
}
});

function lazyLoadGoogleMap() {
    $.getScript("http://maps.google.com/maps/api/js?key=AIzaSyCihkWMXIofb32QIHTIvdKMTMQewjn4ieg&sensor=true&callback=initializeMap")
        .done(function(script, textStatus) {
            //alert("Google map script loaded successfully");
        })
        .fail(function(jqxhr, settings, ex) {
            //alert("Could not load Google Map script: " + jqxhr);
        });
}

function initializeMap() {
    MapInit();
}