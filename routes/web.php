<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as'      => 'home',
    'uses'    => 'site\SiteController@index'
]);


Route::group(['prefix' => 'manage', 'middleware' => 'role:superadministrator|administrator'], function () {
    Route::get('/', 'cms\AdminController@get_index')->name('site');
    Route::resource('/users', 'cms\UserController');
    Route::resource('/posts', 'cms\PostController');
    Route::resource('/pages', 'cms\PageController');    
    Route::resource('/contents', 'cms\ContentController');        
    Route::resource('/describing', 'cms\DescribingController');

    //CONTENS
    Route::resource('/banners', 'cms\BannerController');
    Route::resource('/brands', 'cms\BrandController');    
    Route::resource('/generic', 'cms\GenericController');
    Route::resource('/forum', 'cms\ForumController');
    Route::resource('/packets', 'cms\PacketController');    
    
    Route::get('/menu', 'cms\AdminController@getmenu')->name('menu');
    Route::post('/menu', 'cms\AdminController@postmenu');
    Route::get('/settings', 'cms\AdminController@get_settings')->name('settings');
    Route::post('/settings', 'cms\AdminController@post_settings');
    Route::post('/ajax', 'cms\AjaxController@post');
    Route::get('/media', function(){ return view('cms.media.index');})->name('media');
    Route::get('/media/files', function(){ return view('cms.media.files');})->name('media-files');

    // Other Link
    Route::get('posts/delete/{post_id}', ['uses' => 'cms\PostController@delete', 'as' => 'delete-post']);
    Route::get('banners/delete/{banner_id}', ['uses' => 'cms\BannerController@delete', 'as' => 'delete-banner']);
    Route::get('brands/delete/{brand_id}', ['uses' => 'cms\BrandController@delete', 'as' => 'delete-brand']);
    
    
    Route::get('/datatable/posts', ['uses' => 'cms\PostController@datatable', 'as' => 'posts.datatable']);
    Route::get('/datatable/banners', ['uses' => 'cms\BannerController@bannerDatatable', 'as' => 'banner.datatable']);
    Route::get('/datatable/brands', ['uses' => 'cms\BrandController@brandDatatable', 'as' => 'brand.datatable']);    
    Route::get('/datatable/generic', ['uses' => 'cms\GenericController@genericDatatable', 'as' => 'generic.datatable']);
    Route::get('/datatable/forum', ['uses' => 'cms\ForumController@forumDatatable', 'as' => 'forum.datatable']);
    Route::get('/datatable/packets', ['uses' => 'cms\PacketController@packetDatatable', 'as' => 'packet.datatable']);    

});
Auth::routes();


// Catch all page controller (place at the very bottom)
if (!Request::is('laravel-*') && !Request::is('photos/')){

    //Static Pages
    Route::get('forum', 'site\ForumController@index');
    Route::get('forum/{slug}', 'site\ForumController@show');    
    Route::get('iletisim', 'site\SiteController@contact');
    Route::get('blog', 'site\BlogController@index');
    Route::get('blog/{slug}', 'site\BlogController@show');
    Route::get('blog/{slug}/amp', 'site\BlogController@amp');
    Route::get('/sitemap', 'site\SitemapController@index');

    //Forms
    Route::get('form/{slug1}', 'site\FormController@index');

    Route::post('form', 'site\FormController@send')->name('form-send');

    //All Urls
    Route::get('{slug1}/{slug2?}/{slug3?}/{slug4?}', ['uses' => 'site\SiteController@getPage'])->where('slug', '([A-Za-z0-9\-\/]+)');
}

Route::get('/laravel-clear', function () {   
    echo "Cache temizlendi!";
});