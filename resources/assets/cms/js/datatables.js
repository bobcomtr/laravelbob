
function setDatatable($url, $columns, $rank = false,$model) {

    var settings = {
        processing: true,
        serverSide: true,
        responsive: true,
        "language": {
			"sDecimal":        ",",
			"sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
			"sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
			"sInfoEmpty":      "Kayıt yok",
			"sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
			"sInfoPostFix":    "",
			"sInfoThousands":  ".",
			"sLengthMenu":     "Sayfada _MENU_ kayıt göster",
			"sLoadingRecords": "Yükleniyor...",
			"sProcessing":     "İşleniyor...",
			"sSearch":         "Ara:",
			"sZeroRecords":    "Eşleşen kayıt bulunamadı",
			"oPaginate": {
				"sFirst":    "İlk",
				"sLast":     "Son",
				"sNext":     "Sonraki",
				"sPrevious": "Önceki"
			},
			"oAria": {
				"sSortAscending":  ": artan sütun sıralamasını aktifleştir",
				"sSortDescending": ": azalan sütun sıralamasını aktifleştir"
			}
        },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tümü"]]
    };
    settings.ajax = $url;
    settings.columns = $columns;

    if ($rank) {
        settings.rowReorder = {
            dataSrc: 'rank'
        };
    }
    var Dtable = $('#datatable').DataTable(settings);
    $.fn.dataTable.ext.errMode = 'none';

    if ($rank) {
        $('#datatable').addClass("Sirala");
        Dtable.on('row-reorder', function (e, diff, edit) {
            var myArray = [];
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = Dtable.row(diff[i].node).data();
                myArray.push({
                    id: rowData.id,
                    rank: diff[i].newData
                });
            }
            var jsonString = JSON.stringify(myArray);          
            $.post(config.ajaxpath, { process: 'NewRowPosition', ids: jsonString,model:$model }, function (data) {
                $('#datatable').DataTable().ajax.reload();
            })
        });
    }

}
