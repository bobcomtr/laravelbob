var Bob = {
    FORM: {

        SlugGenerator: function () {
            /*Slug Generator*/
            if($("[data-js*='slug']").length > 0) {
                $("input[name='title']").keyup(_.debounce(function () {
                    $.post(config.ajaxpath, {
                        process: "UrlSlug",
                        title: $(this).val(),
                        lang: $('input[name="lang"]').val(),
                        type: $('input[name="type"]').val()
                    }, function (data) {
                        $("input[name='slug']").val(data);
                    })
                },500))
            }
        },
        SweetAlert() {
            /*AJAX FORM ALERT*/
            if($("body").find("#form").length > 0) {
                $('#form').ajaxForm(function (response) {
                    swal(
                        response.title,
                        response.message,
                        response.status
                    ).then((value) => {
                        if(response.redirect){                          
                            window.location.href = response.redirect;
                          }
                    });
                });
            }
        },
        Summernote: function () {
            var lfm = function (options, cb) {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = cb;
            };

            var LFMButton = function (context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'Image',
                    click: function () {

                        lfm({type: 'image', prefix: '/laravel-filemanager'}, function (url, path) {
                            context.invoke('insertImage', url);
                        });

                    }
                });
                return button.render();
            };
            if($("[data-js*='summernote']").length > 0) {
                $('#summernote-editor').summernote({
                    height: 300,
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color', 'accordion']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', ['lfm'], 'video']],
                        ['view', ['fullscreen', 'codeview', 'help']],

                    ],buttons: {
                        lfm: LFMButton
                    },
                    popover: {
                        image: [
                            ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                            ['float', ['floatLeft', 'floatRight', 'floatNone']],
                            ['remove', ['removeMedia']]
                        ],
                        link: [
                            ['link', ['linkDialogShow', 'unlink']]
                        ],
                        air: [
                            ['color', ['color']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['para', ['ul', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture']]
                        ]
                    },
                    prettifyHtml: true,
                    codemirror: {
                        mode: 'text/html',
                        htmlMode: true,
                        lineNumbers: false,
                        theme: 'monokai'
                      }
                })
            }
        },
        TagsInput:function () {
            $('.custom-tag-input').tagsinput({});
        },
        formvalidateclass:function(){
            jQuery.validator.addClassRules({
                required: {
                  required: true
                },
                zip: {
                  required: true,
                  digits: true,
                  minlength: 5,
                  maxlength: 5
                }
              });
        },
        dateRangePicker:function(){
            if($('body').find('#daterangepicker').length>0){
                
                $('#daterangepicker').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'DD/MM/YYYY'
                }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });
            }
        }
    }
}

$(document).ready(function () {
    moment.locale('tr');
    Bob.FORM.SlugGenerator();
    Bob.FORM.SweetAlert();
    Bob.FORM.Summernote();
    Bob.FORM.TagsInput();
    Bob.FORM.dateRangePicker();
    Bob.FORM.formvalidateclass();

});