$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    $(".getAltCategories").on('click', function () {
        $.post(config.ajaxpath, {
            process: "getAltCategories",
            id: $(this).data('id')
        }, function (data) {
            if (data.length > 0) {
                var compiled = _.template('<% _.forEach(categories, function(category) { %><tr><td><%- category.title %></td>' +
                    '<td><a href="/manage/posts/<%- category.id %>" class="btn btn-warning">Görüntüle</a>' +
                    '<a href="/manage/posts/<%- category.id %>" class="btn btn-danger">Sil</a></td></tr><% }); %>');
                $(".getdata").html(compiled({
                    'categories': data
                }));
            } else {
                $(".getdata").html("içerik yok");
            }
        })
    });

    $(".CategoryType").on('change', function () {
        var selectId = $(this).val();
        $.post(config.ajaxpath, {
            process: "getAltCategories",
            id: selectId
        }, function (data) {
            if (data.length > 0) {
                var compiled = _.template('<% _.forEach(categories, function(category) { %><option value="<%- category.id %>"><%- category.title %></option><% }); %>');
                $("select.Category").empty().trigger("select2.change");
                $('select.Category').prepend('<option value="' + selectId + '">Üst Kategori</option>')
                $('select.Category').append(compiled({
                    'categories': data
                })).trigger('change');

            } else {
                $("select.Category").empty().trigger("select2.change");
                $('select.Category').prepend('<option value="' + selectId + '">Üst Kategori</option>').trigger('change');
            }
        })
    });
    var ids = [];
    $(document).on('click', '.pagelist .link', function () {
        $_this = $(this).parent().parent().parent();
        $selectId = $(this).data('id');
        console.log($selectId);
        $_title = $(this).text();
        $(this).parents('.pagelist').find("a").removeClass('selected');
        $(this).addClass('selected');
        if ($_this.next().hasClass('other')) {
            $('.other').html("");
        } else {
            $_this.nextAll().remove();
        }

        $.post(config.ajaxpath, {
            process: 'getAltPages',
            id: $selectId
        }, function (data) {

            if (data.length > 0) {
                var compiled = _.template($("#pagelist-Template").html());
                $('.other').append(compiled({
                    'pages': data
                }));
            } else {
                $_this.next().html("");
                var compiled = _.template($("#pagelist-Template").html());
                $('.other').append(compiled({
                    'pages': ''
                }));
            }
            $('.other .pagelist:last-child .title').text($_title);
            $('.other .pagelist:last-child .addPage').attr('href', "/manage/pages/create?parentid=" + $selectId + "&crm=2q90rmc2r294352v90mtu029cm924u93");
            $('.other .pagelist:last-child .addPage').attr('data-id', $selectId);
        })
        setTimeout(function () {
            $('.other .pagelist').addClass('active');
        }, 500);
        //$_parent=$(this).parents('.pagelist');
        //$('.panel-body').append('<div class="pagelist"><ul><li><a href="#">ad</a></li><li><a href="#">ad</a></li></ul></div>')
    })
    $('.delete-btn').on('click', function () {
        var url = $(this).attr('href');
        swal({
                title: "Silmek istediğinden emin misin?",
                text: "Bu işlemden sonra verilerin tamamen silinecek",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'DELETE', // user.destroy
                        success: function (result) {
                            swal("Silme işlemi başarılıyla gerçekleşti. Yönlendiriliyorsun...", {
                                icon: "success",
                            }).then((value) => {
                                if (result.redirect) {
                                    window.location.href = result.redirect;
                                }
                            });
                        }
                    });
                }
            });

        return false;
    })

});