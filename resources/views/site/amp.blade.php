<!doctype html>
<html ⚡ lang="tr">
<head>
    <meta charset="utf-8" />
    <meta property="fb:pages" content="409253229245979" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="canonical" href="/blog/{{$url}}">
    <link rel="shortcut icon" href="/site/img/amp_favicon.png">
    <title>{{$post->title}}</title>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    
    <style amp-custom>
        body {
            width: auto;
            margin: 0;
            padding: 0;
        }
        header {
            background: white;
            color: white;
            font-size: 2em;
            text-align: center;
        }
        h1 {
            margin: 0;
            padding: 0.5em;            
            text-align:center;
            color:white;
            font-size:21px;
        }
        .h1box{
            height:150px;
            width:100%;
            background:#661704;
            display: flex;
            flex-flow: column;
            justify-content: center;
            height: 100%;
        }
        p {
            padding: 0.5em;
            margin: 0.5em;
        }
        .orjbutton {
            background-color: transparent;
            color: #ed1c24;
            font-weight: 600;
            line-height: 20px;
            padding: 15px 30px;
            border-radius: 5px;
            border: 1px solid #353535;
            display: inline-block;
            text-decoration: none;
            left: 50%;
            transform: translateX(-50%);
            margin-bottom: 20px;
            position: relative;
        }
        .orjbutton:hover {
            background-color: #353535;
            color: #fff;
        }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
</head>
<body>
    <header>
        <a href="/"><amp-img src="/site/img/logo.png" width="285" height="102"></amp-img></a>       
    </header>
    <article>
        <div class="h1box"><h1>{{$post->title}}</h1></div>
        
        <amp-img src="@isset( $Details['image']){{$Details['image']}} @endisset" layout="responsive" width="285" height="160"></amp-img>
        <?php  
$content = $post->content;
$result = preg_replace('/(<img[^>]+>(?:<\/img>)?)/i', '', $content);
echo $result = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $result);
        ?>
    </article>
    <a class="orjbutton" href="/{{$url}}">Orjinal Sayfaya gitmek için tıklayın.</a>
</body>
</html>