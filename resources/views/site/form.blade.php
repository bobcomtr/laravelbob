@extends('site.app') 
@section('title')
@endsection
 
@section('metakey')
@endsection
 
@section('metadesc')
@endsection
 
@section('icerik')
<section class="page-content">
    <div class="page-content-top">
        <div class="page-content-top-title">
            <div class="container">
                <h1 class="animated hinge fadeInUp">{{$title}} Sigortası Teklif Formu</h1>
            </div>
        </div>
    </div>
    <div class="container">       
        <div class="content">
            <div class="blog-detail">   
                @if(View::exists('forms.'.$form))             
                @include('forms.'.$form)
                @else
                @include('site.404')
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('js')
