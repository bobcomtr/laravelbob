@extends('site.app') 
@section('title'){{ config('settings.sitename') }}
@endsection
 
@section('metakey'){{ config('settings.meta_keyword')
}}
@endsection
 
@section('metadesc'){{ config('settings.meta_desc') }}
@endsection
 
@section('icerik')
<section class="banner" data-id="banner" id="banner">
    <div class="banner-main">
        <div class="row">
            <div class="container-fluid">
                <div id="mainslide" class="owl-carousel">
                    @foreach($banner as $b) @foreach($b->details as $detail)
                    <?php $Bannerdetail[$detail->key]=$detail->value ?> @endforeach
                    <div class="banner-main-item">
                        <img alt="{{ $b->title }}" src="{{$Bannerdetail['image']}}">
                        <div class="banner-main-item-text">
                            <div class="container">
                                <div class="banner-main-item-text-sub">
                                    <h3 class="banner-main-item-text-sub-title" style="color:#fff;">{!! $b->excerpt !!}</h3>
                                    <p class="banner-main-item-text-sub-description">{!! $b->content !!}</p>
                                   
                                    @if(isset($Bannerdetail['buttonurl']))
                                        <a href="{{$Bannerdetail['buttonurl']}}" role="button" class="banner-main-item-text-sub-button">{{$Bannerdetail['buttontext']}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section class="axabid" data-id="axabid" id="axabid">
    <div class="container">
        {{--  <div class="main-title">
            <h3>AXA SİGORTA</h3>
        </div>  --}}
        {!! config('generic.anasayfa-axa-box') !!}
    </div>
</section>
<section class="help" data-id="help" id="help">
    <div class="container">
        {!! config('generic.anasayfa-yardim') !!}
    </div>
</section>
<section class="insurance main-insurance" data-id="insurance" id="insurance">
    <div class="container">
        <div class="main-title">
            <h3>SİGORTA PAKETLERİMİZ</h3>
        </div>
        <div class="row">
           @foreach($packets as $paket) @foreach($paket->details as $detail)
            <?php $paketDetails[$detail->key]=$detail->value ?> @endforeach
            <figure class="insurance-col col-md-3 col-sm-4 col-xs-12">
                <div class="insurance-col-image">
                    <img class="lazy" alt="{{$paket['title']}}" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="{{$paketDetails['image']}}">
                </div>
                <figcaption class="insurance-col-text">
                    <span style="margin-bottom:15px;" class="insurance-col-text-title">{{$paket['title']}}</span>
                    <span style="margin-bottom:15px;" class="insurance-col-text-description">{{$paket['content']}}</span>

                    <a href="{{$paketDetails['buttonurl']}}" role="button" class="insurance-col-text-button">TEKLİF AL</a>
                </figcaption>
            </figure>
            @endforeach
        </div>
    </div>
</section>
<section class="blogs main-blogs" data-id="blogs" id="blogs">
    <div class="container">
        <div class="main-title">
            <h3>BLOG</h3>
        </div>
        <div class="row">
            <?php use App\Category; ?> @foreach($blogs as $blog) 
            <?php if($blog->details){
            foreach($blog->details as $detail){
                $blogDetails[$detail->key]=$detail->value;     
                $category=Category::findOrFail($blogDetails['category']);
                $blogDetails['cattitle']=$category->title;
            }
                } ?>   

                <figure class="blogs-col col-md-3 col-sm-4 col-xs-12">
                    <a href="/blog/{{$blog['slug']}}">
                            <span class="blogs-col-image">
                                <svg class="icon icon-plus">
                                    <use xlink:href="#icon-plus"></use>
                                </svg>
                                <img class="lazy" alt="@isset($blogDetails['cattitle']) {{$blogDetails['cattitle']}}@endisset" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="@isset($blogDetails['image']) {{$blogDetails['image']}} @endisset">
                            </span>
                            <figcaption class="blogs-col-text">
                                <span class="blogs-col-text-top">
                                    <time class="blogs-col-text-top-time"> {{date('d/m/Y', strtotime( $blog['published_at']))}}
                                    </time>
                                    <span class="blogs-col-text-top-category">@isset($blogDetails['cattitle']){{$blogDetails['cattitle']}}@endisset</span>
                                </span>
                                <span class="blogs-col-text-title">{{$blog['title']}}</span>
                                <span class="blogs-col-text-description">{{$blog['excerpt']}}</span>
                            </figcaption>
                        </a>
                </figure>
                @endforeach
        </div>
    </div>
</section>
<section class="logos" data-id="logos" id="logos">
    <div class="logo-main">
        <div class="container">
            <div class="row">
                <div id="logoslide" class="owl-carousel">
                    @foreach($brand as $sirket) @foreach($sirket->details as $detail)
                    <?php $brandDetail[$detail->key]=$detail->value ?> @endforeach
                    <div class="item">
                        <a href="{{$brandDetail['buttonurl']}}">
                            <img class="lazy" alt=" {{$sirket->title}}" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="{{$brandDetail['image']}}">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('css')
@endsection
 
@section('js')
<script>
    $(function(){
            var $modaltimes='{!! config("settings.modaldate") !!}'.split(' - ');    
            var $modalshow='{!! config("settings.modalshowtype") !!}';
            var $modaltitle='{{ config("settings.modaltitle") }}';
            var $modalcontent='{!! config("settings.modalcontent") !!}';
            moment.locale('tr');
            var e= moment($modaltimes[1], "DD/MM/YYYY");
            var s= moment($modaltimes[0], "DD/MM/YYYY");
            var n= moment(new moment().format("DD/MM/YYYY"),"DD/MM/YYYY");
            var k= n.diff(e,'day');
            var g=n.diff(s,'day');
          
            if($modaltimes && $modalshow && $modaltitle!="" && $modalcontent!=""){
                if(g>=0 && k<=0){
                    if(localStorage.modal=='1' && $modalshow=="1"){
                        return false;
                   }        
                    localStorage.setItem("modal", $modalshow);
                    $('#modal .modal-title').html($modaltitle);
                    $('#modal .modal-body').html($modalcontent);
                    $("#modal").modal();
                }
            }
        })
</script>
@endsection