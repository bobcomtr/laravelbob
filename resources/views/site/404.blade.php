@extends('site.app')
@section('icerik')
 
<section class="page-content">      
        <div class="container">
            <div class="content text-center">
                <div class="error">404</div>
                <h3>Üzgünüz, aradığınız sayfa mevcut değil.</h3>
            </div>
        </div>
    </section>
    
  @endsection
  @section('css')
  @endsection
  @section('js')
  @endsection