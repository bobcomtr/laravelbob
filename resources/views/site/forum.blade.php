@extends('site.app') 
@section('title') @isset($Details['metatit']) {{$Details['metatit']}} @endisset @endsection
@section('metakey')@isset($Details['metakey']) {{$Details['metakey']}} @endisset @endsection
@section('metadesc')@isset($Details['metadesc']) {{$Details['metadesc']}} @endisset @endsection
@section('icerik')
<section class="page-content">
        <div class="page-content-top">
            <div class="page-content-top-title">
                <div class="container">
                    <h1>{{$post->title}}</h1>
                </div>
            </div>
        </div>
        <div class="container">       
            <div class="content">
                <div class="blog-detail">                   
                    {!!$post->content!!}
                </div>
                <div id="disqus_thread"></div>
            </div>
        </div>
    </section>

  @endsection
  @section('css')
  <style>
        #disqus_thread {
            position: relative;
        }
        #disqus_thread:after {
            content: "";
            display: block;
            height: 55px;
            width: 100%;
            position: absolute;
            bottom: 0;
            background: white;
        }
  </style>
  @endsection
  @section('js')
  <script id="dsq-count-scr" src="//canatar-sigorta.disqus.com/count.js" async></script>
  <script>
                    
        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://canatar-sigorta.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    
  @endsection