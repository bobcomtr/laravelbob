@extends('site.app')
@section('title') CanAtar Sigorta Blog @endsection
@section('metakey') CanAtar Sigorta Blog @endsection
@section('metadesc') CanAtar Sigorta Blog @endsection
@section('icerik')

<section class="page-content">
    <div class="page-content-top">
        <div class="page-content-top-title">
            <div class="container">
                <h1>Blog  </h1>
            </div>
        </div>
    </div>
    <div class="container">
        
       <?php
       use App\Category;
       ?>
        <div class="content">
            <div class="blogs">
                    @foreach($posts as $blog) 
                    <?php if($blog->details){
                    foreach($blog->details as $detail){
                        $blogDetails[$detail->key]=$detail->value;     
                        $category=Category::findOrFail($blogDetails['category']);
                        $blogDetails['cattitle']=$category->title;
                    }
                        } ?>   
        
                <figure class="blogs-col col-md-3 col-sm-4 col-xs-12">
                    <a href="/blog/{{$blog->slug}}">
                        <span class="blogs-col-image">
                            <svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg>
                            <img src="@isset($blogDetails['image']) {{$blogDetails['image']}} @endisset">
                        </span>
                        <figcaption class="blogs-col-text">
                            <span class="blogs-col-text-top">
                                <time class="blogs-col-text-top-time">{{date('d/m/Y', strtotime( $blog->published_at))}}</time>
                                <span class="blogs-col-text-top-category">@isset($blogDetails['cattitle']) {{$blogDetails['cattitle']}}@endisset</span>
                            </span>
                            <span class="blogs-col-text-title">{{$blog->title}}</span>
                            <span class="blogs-col-text-description">{{$blog->excerpt}}</span>
                      </figcaption>
                    </a>
                </figure>
              @endforeach
                
               <div class="clearfix"></div>
                <nav aria-label="Page navigation">
                    {{ $posts->links() }}
                </nav>
            </div>
            </div>
        </div>
        
    </div>
</section>

  @endsection
  @section('css')
  @endsection
  @section('js')
 
  @endsection