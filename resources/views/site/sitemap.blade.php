<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ url('/') }}</loc>
        <lastmod>{{date('c',time())}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1.00</priority>
    </url>
    <url>
        <loc>{{ url('/').'/blog' }}</loc>
        <lastmod>{{date('c',time())}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{ url('/').'/forum' }}</loc>
        <lastmod>{{date('c',time())}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{ url('/').'/iletisim' }}</loc>
        <lastmod>{{date('c',time())}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.80</priority>
    </url>
  {!!$sitemap!!}

    {{--  @foreach ($makaleler as $makale)    
    <url>
        <loc>{{ url('/').'/'.$makale->slug }}</loc>
        <lastmod>{{ date('c',strtotime($makale->published_at)) }}</lastmod>
        <changefreq>never</changefreq>
        <priority>0.60</priority>
    </url>
    @endforeach  --}}
</urlset>