@extends('site.app') 
@section('title') CanAtar Sigorta Forum @endsection
@section('metakey') CanAtar Sigorta Forum @endsection
@section('metadesc') CanAtar Sigorta Forum @endsection
@section('icerik')

<section class="page-content">
    <div class="page-content-top">
        <div class="page-content-top-title">
            <div class="container">
                <h1>Forum</h1>
            </div>
        </div>
    </div>
    <div class="container">

        <?php
       use App\Category;
       ?>
            <div class="content">
                <div class="blogs">
                    <table class="table table-zebra table-responsive table-striped">
                        <thead class="thead-dark">  
                            <tr>
                                <th>Başlık</th>
                                <th>Tarih</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                            <tr>
                                <td style="width:85%;">
                                        <i class="fa fa-bookmark"></i>
                                    <a href="/forum/{{$post->slug}}"><b>{{$post->title}}</b></a>
                                    <br>
                                    <span style="font-size:13px;"> {{$post->excerpt}}</span>
                                </td>
                                <td >
                                    <time class="">{{date('d/m/Y h:i:s', strtotime( $post->published_at))}}</time>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <nav aria-label="Page navigation">
                        {{ $posts->links() }}
                    </nav>
                </div>
            </div>
    </div>

    </div>
</section>

@endsection @section('css') @endsection @section('js') @endsection