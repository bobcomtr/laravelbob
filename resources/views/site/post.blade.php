@extends('site.app') 
@section('title') @isset($Details['metatit']) {{$Details['metatit']}} @endisset @endsection
@section('metakey')@isset($Details['metakey']) {{$Details['metakey']}} @endisset @endsection
@section('metadesc')@isset($Details['metadesc']) {{$Details['metadesc']}} @endisset @endsection
@section('ampcanonical')<link rel="amphtml" href="/blog/{{$url}}/amp">@endsection
@section('image')@isset( $Details['image']){{$Details['image']}}@endisset @endsection

@section('icerik')

<section class="page-content">
    <div class="page-content-top">
        <div class="page-content-top-title">
            <div class="container">
                <h1>{{$post->title}}</h1>
            </div>
        </div>
    </div>
    <div class="container">       
        <div class="content">
            <div class="blog-detail">
                <div class="blog-detail-image">
                    <img src="@isset( $Details['image']){{$Details['image']}} @endisset">
                </div>
                {!!$post->content!!}
            </div>
        </div>
    </div>
</section>
  @endsection
  @section('css')
  @endsection
  @section('js')
 
  @endsection