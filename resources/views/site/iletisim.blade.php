@extends('site.app') @section('title') @isset($Details['metatit']) {{$Details['metatit']}} @endisset @endsection @section('metakey')@isset($Details['metakey'])
{{$Details['metakey']}} @endisset @endsection @section('metadesc')@isset($Details['metadesc']) {{$Details['metadesc']}} @endisset
@endsection @section('icerik')

<section class="page-content">
    <div class="page-content-top">
        <div class="page-content-top-title">
            <div class="container">
                <h1>İletişim</h1>
            </div>
        </div>
    </div>
    <div class="container">


        <div class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="map" class="map">

                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3>İletişim Bilgileri</h3>
                    <address class="address">
                        <p>
                            <strong>{{config("settings.maptitle")}}</strong>
                        </p>
                        <p>{{config("settings.adres")}}
                        </p>
                        <p class="address-phone">
                            <a href="https://api.whatsapp.com/send?phone=9{!! str_replace(' ','',config('settings.whatsapp')) !!}&text=Merhabalar, yardımcı olur musunuz ?">
                                <svg class="icon icon-whatsapp">
                                    <use xlink:href="#icon-whatsapp"></use>
                                </svg>
                                <span>{{config('settings.whatsapp')}}</span>
                            </a>
                        </p>
                        <p class="address-phone">
                            <a href="tel:{{config('settings.tel')}}">
                                <svg class="icon icon-phone-red">
                                    <use xlink:href="#icon-phone-red"></use>
                                </svg>
                                <span>{{config('settings.tel')}}</span>
                            </a>
                        </p>
                    </address>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <h3>İletişim Formu</h3>
                    @include('forms.contact')                    
                </div>
            </div>
        </div>
    </div>
</section>

@endsection @section('css') @endsection 


@section('js') 
<script>
    var locations = [
        ['{{config("settings.maptitle")}}','{{config("settings.lat")}}','{{config("settings.long")}}', 2, '{{config("settings.adres")}}'],
    ];
    function lazyLoadGoogleMap() {
        $.getScript("https://maps.google.com/maps/api/js?v=3.5&key={{config('settings.mapsapi')}}&sensor=true&callback=initializeMap")
            .done(function(script, textStatus) {
                //alert("Google map script loaded successfully");
            })
            .fail(function(jqxhr, settings, ex) {
                //alert("Could not load Google Map script: " + jqxhr);
            });
    }
</script>
<script src="/site/js/map.js"></script>
@endsection