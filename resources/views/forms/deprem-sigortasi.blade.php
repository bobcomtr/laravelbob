@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Deprem Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Ünvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Ünvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadınız</label>
            <input type="text" class="form-control required" name="soyadiniz" id="surname" placeholder="Soyadınız">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12"><label>Başlangıç Tarihi</label>
        <div class="form-group required">
            <div id="datepicker-component" class="input-group date">
                <input type="text" class="form-control required" name="baslangic-tarihi"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12"><label>Bitiş Tarihi</label>
        <div class="form-group required">
            <div id="datepicker-component" class="input-group date">
                <input type="text" class="form-control required" name="bitis-tarihi"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Adres</label>
            <input type="text" class="form-control" name="adres" id="adres" placeholder="Adres">
        </div>
    </div>
   
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Toplam Kat Sayısı</label>
            <input type="text" class="form-control required" name="toplam-kat-sayisi" id="toplam-kat-sayisi" placeholder="Toplam Kat Sayısı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Kat Bilginiz</label>
            <input type="text" class="form-control required" name="kat-bilginiz" id="kat-bilginiz" placeholder="Kat Bilginiz">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Hasar Durumu</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="hasar-durumu" id="hasar-durumu" value="Hasarlı" checked>
                        Hasarlı
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="hasar-durumu" id="hasar-durumu" value="Hasarsız">
                        Hasarsız
                    </label>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>İnşa Tarzı</label>
            <select class="form-control required" name="insa-tarzi" id="insatarzi">
                <option value="" selected="true" id="meslek-0">Seçiniz</option>
                <option value="Çelik">Çelik</option>
                <option value="Betonarme">Betonarme</option>
                <option value="Kalas">Kalas</option>
                <option value="Diğer">Diğer</option>

            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>İnşaa Tarzı Diğer ise yazınız</label>
            <input type="text" class="form-control" name="insa-tarzi-diger" id="insa-tarzi-diger" placeholder="İnşaa Tarzı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Kullanım Şekli</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="kullanim-sekli" id="kullanim-sekli" value="Hasarlı" checked>
                        Konut
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="kullanim-sekli" id="kullanim-sekli" value="Hasarsız">
                        İşyeri
                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Bina İnşaa Yılı</label>
            <input type="text" class="form-control required" name="insa-yili" id="insa-yili" placeholder="Bina İnşaa Yılı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Brüt (M<sup>2</sup>)</label>
            <input type="text" class="form-control required" name="plaka" id="plaka" placeholder="Brüt (Metre Kare)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label>Eski Poliçe No veya DASK Seri No	</label>
                <input type="text" class="form-control" name="eski-police" id="eskipolice" placeholder="Eski Poliçe No veya DASK Seri No">
            </div>
        </div>
   
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}