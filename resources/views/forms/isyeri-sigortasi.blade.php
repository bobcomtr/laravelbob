@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="İşyeri Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Unvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Unvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadı</label>
            <input type="text" class="form-control required" name='soyadi' id="soyadi" placeholder="Soyadı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Vergi Numaranız </label>
            <input type="text" class="form-control" name="vergi-no" id="vergi-no" placeholder="Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Riziko Adresi</label>
            <input type="text" class="form-control required" name="riziko-adresi" id="riziko-adresi" placeholder="Riziko Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Faaliyet Konusu</label>
            <input type="text" class="form-control required" name="faaliyet-konusu" id="faaliyet-konusu" placeholder="Faaliyet Konusu">
        </div>
    </div>
    
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Bulunduğu Kat</label>
            <input type="text" class="form-control required" name="bulundugu-kat" id="bulundugu-kat" placeholder="Bulunduğu Kat">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Bina m<sup>2</sup></label>
            <input type="text" class="form-control required" name="metre-kare" id="metre-kare" placeholder="Binanın Metrekaresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Bina Sigorta Bedeli</label>
            <input type="text" class="form-control required" name="bina-sigorta-bedeli" id="bina-sigorta-bedeli" placeholder="Bina Sigorta Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Emtia Bedeli</label>
            <input type="text" class="form-control" name="emtia-bedeli" id="emtia-bedeli" placeholder="Emtia Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group ">
            <label>Makina Tesisat Bedeli</label>
            <input type="text" class="form-control" name="makina-tesisat-bedeli" id="makina-tesisat-bedeli" placeholder="Makina Tesisat Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group ">
            <label>Demirbaş Bedeli</label>
            <input type="text" class="form-control" name="demirbas-bedeli" id="demirbas-bedeli" placeholder="Demirbaş Bedeli">
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Dekorasyon Bedeli</label>
            <input type="text" class="form-control" name="dekorasyon-bedeli" id="dekorasyon-bedeli" placeholder="Dekorasyon Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group ">
            <label>ECS</label>
            <input type="text" class="form-control" name="ecs" id="ecs" placeholder="ECS">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group ">
            <label>Cam Kırılması</label>
            <input type="text" class="form-control" name="cam-kirilmasi" id="cam-kirilmasi" placeholder="Cam Kırılması">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Bina İnşa Tarzı</label>
            <input type="text" class="form-control" name="bina-insa-tarzi" id="bina-insa-tarzi" placeholder="Bina İnşa Tarzı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Kasa Muhteviyatı</label>
            <input type="text" class="form-control" name="kasa-muhteviyatı" id="kasa-muhteviyatı" placeholder="Kasa Muhteviyatı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Riziko Türü</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="riziko-turu" id="riziko-turu" value="Ticari" checked>
                        Ticari
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="riziko-turu" id="riziko-turu" value="Sinai">
                        Sinai
                    </label>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-6">
        <div class="form-group">
            <label>Hırsızlık Önlemleri</label>
            <textarea class="form-control" rows="3" name="hırsızlık-onlemleri" id="hırsızlık-onlemleri" placeholder="Hırsızlık Önlemleri"></textarea>
        </div>
    </div>
    <div class="col-xs-12  col-sm-6">
        <div class="form-group">
            <label>Yangın Önlemleri</label>
            <textarea class="form-control" rows="3" name="yangin-onlemleri" id="yangin-onlemleri" placeholder="Yangın Önlemleri"></textarea>
        </div>
    </div>
    <div class="col-xs-12  col-sm-6">
        <div class="form-group">
            <label>Sel Seylan Detayı</label>
            <textarea class="form-control" rows="3" name="sel-detayi" id="sel-detayi" placeholder="Sel Seylan Detayı"></textarea>
        </div>
    </div>
    <div class="col-xs-12  col-sm-6">
        <div class="form-group">
            <label>Emtianın Depolanış Şekli</label>
            <textarea class="form-control" rows="3" name="emtia-depolama-sekli" id="emtia-depolama-sekli" placeholder="Emtianın Depolanış Şekli"></textarea>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}