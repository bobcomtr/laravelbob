@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Hayat Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Unvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Unvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadı</label>
            <input type="text" class="form-control required" name='soyadi' id="soyadi" placeholder="Soyadı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12"><label>Doğum Tarihiniz</label>
        <div class="form-group required">
            <div id="datepicker-component" class="input-group date">
                <input type="text" class="form-control required" name="dogum-tarihi"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Baba Adı</label>
            <input type="text" class="form-control required" name="baba-adi" id="baba-adi" placeholder="Baba Adı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ana Adı</label>
            <input type="text" class="form-control required" name="ana-adi" id="ana-adi" placeholder="Ana Adı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tabiyeti</label>
            <input type="text" class="form-control" name="tabiyeti" id="tabiyeti" placeholder="Tabiyeti">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Medeni Durum</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="medeni-durum" id="medeni-durum" value="Evli" checked>
                        Evli
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="medeni-durum" id="medeni-durum" value="Bekar">
                        Bekar
                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Mesleği</label>
            <input type="text" class="form-control" name="meslegi" id="meslegi" placeholder="Mesleği">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Aylık Ortalama Gelir</label>
            <input type="text" class="form-control" name="ortalama-gelir" id="ortalama-gelir" placeholder="Ortalama Gelir">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Aylık Ortalama Gider</label>
            <input type="text" class="form-control" name="ortalama-gider" id="ortalama-gider" placeholder="Ortalama Gider">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Aylık Tasarruf Etmek İstediğin tutar</label>
            <input type="text" class="form-control" name="tasarruf-tutari" id="tasarruf-tutari" placeholder="Tasarruf tutari">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}