@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Konut Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Ünvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Ünvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadınız</label>
            <input type="text" class="form-control required" name="soyadiniz" id="surname" placeholder="Soyadınız">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Adres</label>
            <input type="text" class="form-control" name="adres" id="adres" placeholder="Adres">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>İnşa Tarzı</label>
            <select class="form-control required" name="insa-tarzi" id="insatarzi">
                    <option value="" selected="true" id="meslek-0">Seçiniz</option>
                    <option value="Tam Kagir (Çelik-Betonarme-Karkas)">Tam Kagir (Çelik-Betonarme-Karkas)</option>
                    <option value="Yarı Kagir">Yarı Kagir</option>
                    <option value="Yığma Kagir">Yığma Kagir</option>    
                </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>Binadaki Kat Sayısı</label>
            <select class="form-control required" name="binadaki-kat-sayisi" id="binadaki-kat-sayisi">
                        <option value="" selected="true" id="meslek-0">Seçiniz</option>
                        <option value="1-5 Kat">1-5 Kat</option>
                        <option value="6-10 Kat">6-10 Kat</option>
                        <option value="11-15 Kat">11-15 Kat</option>
                        <option value="15 +">15 +</option>
                    </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>Çatı Tipi</label>
            <select class="form-control required" name="cati-tipi" id="cati-tipi">
                            <option value="" selected="true" id="meslek-0">Seçiniz</option>
                            <option value="Beton veya Beton üzeri oturtma çatı">Beton veya Beton üzeri oturtma çatı</option>
                            <option value="Ahşap veya çelik taşıyıcı, üstü etermit">Ahşap veya çelik taşıyıcı, üstü etermit</option>
                        </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Kat Bilginiz</label>
            <input type="text" class="form-control required" name="kat-bilginiz" id="kat-bilginiz" placeholder="Kat Bilgisi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Koruma Önlemleri</label>
            <div class="clearfix"></div>
            <div class="row">
                <div class="checkbox col-xs-4">
                    <label>
                                <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Panjur">
                                Panjur
                            </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                                <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Demir Kepenk">
                                Demir Kepenk
                            </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                     <input type="checkbox" name="koruma-onlemleri" id="koruma-onlemleri[]" value="Bekçi">
                                    Bekçi
                                </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                                        <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Alarm">
                                        Alarm
                                    </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                                            <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Demir Parmaklık">
                                            Demir Parmaklık
                                        </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                                                <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Güvenlik">
                                                Güvenlik
                                            </label>
                </div>
                <div class="checkbox col-xs-4">
                    <label>
                                                    <input type="checkbox" name="koruma-onlemleri[]" id="koruma-onlemleri" value="Hiçbiri">
                                                   Hiçbiri
                                                </label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>Konutun Kullanım Amacı</label>
            <select class="form-control required" name="cati-tipi" id="cati-tipi">
                                <option value="" selected="true" id="meslek-0">Seçiniz</option>
                                <option value="Sürekli İkametgah">Sürekli İkametgah</option>
                                <option value="Site içinde bulunan tatil evi">Site içinde bulunan tatil evi</option>
                                <option value="Site içinde bulunmayan tatil evi">Site içinde bulunmayan tatil evi</option>                                
                            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Konut (M<sup>2</sup>)</label>
            <input type="text" class="form-control required" name="konut-m2" id="konut-m2" placeholder="Konut (Metre Kare)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>Döviz Cinsi</label>
            <select class="form-control required" name="cati-tipi" id="cati-tipi">
                                <option value="" selected="true" id="meslek-0">Seçiniz</option>
                                <option value="TL">TL</option>
                                <option value="USD">USD</option>
                                <option value="EURO">EURO</option>                                
                            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Bina Bedeli</label>
            <input type="text" class="form-control required" name="bina-bedeli" id="bina-bedeli" placeholder="Bina Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>DASK'a Tabi mi?</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="daska-tabi" id="daska-tabi" value="Evet" checked>
                        Evet
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="daska-tabi" id="daska-tabi" value="Hayır">
                        Hayır
                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Eşya Bedeli</label>
            <input type="text" class="form-control required" name="esya-bedeli" id="esya-bedeli" placeholder="Eşya Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Kıymetli Eşya Bedeli</label>
            <input type="text" class="form-control" name="kiymetli-esya-bedeli" id="kiymetli-esya-bedeli" placeholder="Kıymetli Eşya Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Cam - Ayna Bedeli</label>
            <input type="text" class="form-control" name="cam-ayna-bedeli" id="cam-ayna-bedeli" placeholder="Cam - Ayna Bedeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Elektronik Cihaz Bedeli</label>
            <input type="text" class="form-control" name="elektronik-bedeli" id="elektronik-bedeli" placeholder="Elektronik Cihaz Bedeli (Eşya Bedelinin %10'u ile sınırlıdır)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>İstenen Ek Teminatlar</label>
            <div class="clearfix"></div>
            <div class="row">
                <div class="checkbox col-xs-6">
                    <label>
                                <input type="checkbox" name="ek-teminatlar[]" id="ek-teminatlar" value="Deprem">
                                Deprem
                            </label>
                </div>
                <div class="checkbox col-xs-6">
                    <label>
                                <input type="checkbox" name="ek-teminatlar[]" id="ek-teminatlar" value="Deniz Taşıtları Çarpması">
                                Deniz Taşıtları Çarpması
                            </label>
                </div>
                <div class="checkbox col-xs-6">
                    <label>
                     <input type="checkbox" name="ek-teminatlar[]" id="ek-teminatlar" value="Ferdi Kaza">
                     Ferdi Kaza
                                </label>
                </div>
                <div class="checkbox col-xs-6">
                    <label>
                                        <input type="checkbox" name="ek-teminatlar[]" id="ek-teminatlar" value="Kişisel Eşya/hırsızlık">
                                        Kişisel Eşya/hırsızlık
                                    </label>
                </div>
                <div class="checkbox col-xs-6">
                    <label>
                                            <input type="checkbox" name="ek-teminatlar[]" id="ek-teminatlar" value="3.Şahıs sorumluluk">
                                            3.Şahıs sorumluluk
                                        </label>
                </div>             
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Varsa Eski Sigorta Şirketiniz</label>
            <input type="text" class="form-control" name="eski-sirket" id="eskisirket" placeholder="Varsa Eski Sigorta Şirketiniz">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select">
            <label>İhtiyari Mali Mesuliyet (İMM)</label>
            <select class="form-control" name="kullanım-tarzı" id="kullanimtarzi">
                <option value="" selected="true">Seçiniz</option>
                <option value="Seçenek 1: 25.000 - 75.000 - 25.000">Seçenek 1: 25.000 - 75.000 - 25.000</option>
                <option value="Seçenek 2: 30.000 - 90.000 - 30.000">Seçenek 2: 30.000 - 90.000 - 30.000</option>
                <option value="Seçenek 3: 50.000 - 150.000 - 50.000">Seçenek 3: 50.000 - 150.000 - 50.000</option>
                <option value="Seçenek 4: 100.000">Seçenek 4: 100.000</option>
                <option value="Seçenek 5: 2.500.000">Seçenek 5: 2.500.000</option>
                <option value="Sınırsız">Sınırsız</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select">
            <label>Ferdi Kaza</label>
            <select class="form-control" name="ferdi-kaza" id="kullanimtarzi">
                <option value="" selected="true">Seçiniz</option>
                <option value="Seçenek 1: 5.000 - 5.000">Seçenek 1: 5.000 - 5.000</option>
                <option value="Seçenek 2: 10.000 - 10.000">Seçenek 2: 10.000 - 10.000</option>
                <option value="Seçenek 3: 15.000 - 15.000">Seçenek 3: 15.000 - 15.000</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Asbis Referans No</label>
            <input type="text" class="form-control" name="asbis-referans-no" id="asbis" placeholder="Asbis Referans No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ruhsat Seri No</label>
            <input type="text" class="form-control" name="ruhsat-seri-no" id="ruhsatserino" placeholder="Ruhsat Seri No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}