@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(array('route' => 'form-send', 'id' => 'form')) !!}
<input type="hidden" name='formname' value="İletişim Formu">

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Adınız</label>
            <input type="text" class="form-control required" name='adınız' id="adınız" placeholder="Adınız">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Soyadınız</label>
            <input type="text" class="form-control required" name="soyadınız" id="soyadınız" placeholder="Soyadınız">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Email address</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Konu</label>
            <input type="text" class="form-control required" name="konu" id="konu" placeholder="Konu">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <label>Mesajınız</label>
            <textarea class="form-control required" rows="3" name="mesaj" id="mesaj" placeholder="Mesajınız"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}