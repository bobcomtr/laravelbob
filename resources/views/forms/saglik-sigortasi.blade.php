@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Sağlık Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad Soyad</label>
            <input type="text" class="form-control required" name='ad-soyad' id="name" placeholder="Ad Soyad">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12"><label>Doğum Tarihiniz</label>
        <div class="form-group required">
            <div id="datepicker-component" class="input-group date">
                <input type="text" class="form-control required" name="dogum-tarihi"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>İkamet Ettiğiniz İl</label>
            <input type="text" class="form-control required" name="il" id="il" placeholder="İkamet Ettiğiniz İl">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Boy / Kilo</label>
            <input type="text" class="form-control required" name="boy-kilo" id="boykilo" placeholder="Boy Kilo">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Medeni Durum</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="medeni-durum" id="medeni-durum" value="Evli" checked>
                        Evli
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="medeni-durum" id="medeni-durum" value="Bekar">
                        Bekar
                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>İstenilen Teminat</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                            <input type="radio" name="istenilen-teminat" id="istenilen-teminat" value="Yatarak" checked>
                            Yatarak
                        </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                            <input type="radio" name="istenilen-teminat" id="istenilen-teminat" value="Yatarak+Ayakta">
                            Ayakta + Yatakta
                        </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Doğum Teminatı</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                                <input type="radio" name="dogum-teminati" id="dogum-teminati" value="Evet" checked>
                                Evet
                            </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                                <input type="radio" name="dogum-teminati" id="dogum-teminati" value="Hayır">
                                Hayır
                            </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Yurtdışı Teminatı</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                                    <input type="radio" name="yurtdisi-teminati" id="yurtdisi-teminati" value="Evet" checked>
                                    Evet
                                </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                                    <input type="radio" name="yurtdisi-teminati" id="yurtdisi-teminati" value="Hayır">
                                    Hayır
                                </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Önceden Sağlık Sigortası Varmı</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                                        <input type="radio" name="onceden-saglik-sigortasi" id="onceden-saglik-sigortasi" value="Var" checked>
                                        Var
                                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                                        <input type="radio" name="onceden-saglik-sigortasi" id="onceden-saglik-sigortasi" value="Yok">
                                        Yok
                                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}