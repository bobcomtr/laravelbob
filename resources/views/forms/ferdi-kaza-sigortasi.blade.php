@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Ferdi Kaza Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Unvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Unvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadı</label>
            <input type="text" class="form-control required" name='soyadi' id="soyadi" placeholder="Soyadı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Adres</label>
            <input type="text" class="form-control" name="adres" id="adres" placeholder="Adres">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Meslek Faaliyet Konusu</label>
            <input type="text" class="form-control" name="meslek" id="meslek" placeholder="Meslek Faaliyet Konusu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Kişi Sayısı</label>
            <input type="text" class="form-control" name="kisi-sayisi" id="kisi-sayisi" placeholder="Kişi Sayısı (firmalarda bordrolu çalışan sayısı)">
        </div>
    </div>
    
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>İstenen Kişi başı Ölüm teminatı</label>
            <input type="text" class="form-control" name="olum-teminati" id="olum-teminati" placeholder="İstenen Kişi başı Ölüm teminatı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>İstenen Kişi başı S.Sakatlık teminatı</label>
            <input type="text" class="form-control" name="sakatlik-teminati" id="sakatlik-teminati" placeholder="İstenen Kişi başı S.Sakatlık teminatı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>İstenen Kişi başı tedavi teminatı</label>
            <input type="text" class="form-control" name="tedavi-teminati" id="tedavi-teminati" placeholder="İstenen Kişi başı tedavi teminatı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <label>Deprem Teminatı İsteniyor Mu?</label>
        <div class="clearfix"></div>
        <div class="row">
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="deprem-teminati" id="deprem-teminati" value="Evet" checked>
                        Evet
                    </label>
            </div>
            <div class="radio col-xs-6">
                <label>
                        <input type="radio" name="deprem-teminati" id="deprem-teminati" value="Hayır">
                        Hayır
                    </label>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}