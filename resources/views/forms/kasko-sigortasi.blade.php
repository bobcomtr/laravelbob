@section('title')
@endsection
 @if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif {!! Form::open(['route'=>'form-send','id'=>'form']) !!}
<input type="hidden" name='formname' value="Kasko Sigortası">
<input type="text" class="form-control" name='honeypot' id="honeypot" autocomplete="off" tabindex="-1" style="opacity:0;height:0;">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ad/Ünvan</label>
            <input type="text" class="form-control required" name='ad-unvan' id="name" placeholder="Ad/Ünvan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Soyadınız</label>
            <input type="text" class="form-control required" name="soyadiniz" id="surname" placeholder="Soyadınız">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>TC Kimlik No / Vergi No</label>
            <input type="text" class="form-control required only-number" maxlength="11" name="TCkimlik" id="tckimlik" placeholder="TC Kimlik No / Vergi No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Email adresi</label>
            <input type="email" class="form-control email" name="mail" id="mail" placeholder="Email Adresi">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Telefon(Ev, İş)</label>
            <input type="text" class="form-control required only-number" name="telefon" id="telefon" placeholder="Telefon(Ev, İş)">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Cep Telefonu</label>
            <input type="text" class="form-control required only-number" name="cep-telefonu" id="ceptel" placeholder="Cep Telefonu">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Meslek</label>
            <input type="text" class="form-control required" name="meslek" id="meslek" placeholder="Meslek">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select required">
            <label>Kullanım Tarzı</label>
            <select class="form-control required" name="kullanim-tarzi" id="kullanimtarzi">
                <option value="" selected="true" id="meslek-0">Seçiniz</option>
                <option value="Hususi">Hususi</option>
                <option value="Kamyonet">Kamyonet</option>
                <option value="Minibüs">Minibüs</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Marka</label>
            <input type="text" class="form-control required" name="marka" id="marka" placeholder="Marka">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Araç Modeli</label>
            <input type="text" class="form-control required" name="arac-modeli" id="arac-modeli" placeholder="Araç Modeli">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Model Yılı</label>
            <input type="text" class="form-control required" name="model-yılı" id="model-yili" placeholder="Model Yılı">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Araç Değeri</label>
            <input type="text" class="form-control required" name="araç-degeri" id="arac-degeri" placeholder="Araç Değeri">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Plaka </label>
            <input type="text" class="form-control required" name="plaka" id="plaka" placeholder="Plaka">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12"><label>Araç Tescil Tarihi </label>
        <div class="form-group required">
            <div id="datepicker-component" class="input-group date">
                <input type="text" class="form-control required" name="arac-tescil-tarihi"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Varsa Eski Sigorta Şirketiniz</label>
            <input type="text" class="form-control" name="eski-sirket" id="eskisirket" placeholder="Varsa Eski Sigorta Şirketiniz">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select">
            <label>İhtiyari Mali Mesuliyet (İMM)</label>
            <select class="form-control" name="kullanım-tarzı" id="kullanimtarzi">
                <option value="" selected="true">Seçiniz</option>
                <option value="Seçenek 1: 25.000 - 75.000 - 25.000">Seçenek 1: 25.000 - 75.000 - 25.000</option>
                <option value="Seçenek 2: 30.000 - 90.000 - 30.000">Seçenek 2: 30.000 - 90.000 - 30.000</option>
                <option value="Seçenek 3: 50.000 - 150.000 - 50.000">Seçenek 3: 50.000 - 150.000 - 50.000</option>
                <option value="Seçenek 4: 100.000">Seçenek 4: 100.000</option>
                <option value="Seçenek 5: 2.500.000">Seçenek 5: 2.500.000</option>
                <option value="Sınırsız">Sınırsız</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group fb-select">
            <label>Ferdi Kaza</label>
            <select class="form-control" name="ferdi-kaza" id="kullanimtarzi">
                <option value="" selected="true">Seçiniz</option>
                <option value="Seçenek 1: 5.000 - 5.000">Seçenek 1: 5.000 - 5.000</option>
                <option value="Seçenek 2: 10.000 - 10.000">Seçenek 2: 10.000 - 10.000</option>
                <option value="Seçenek 3: 15.000 - 15.000">Seçenek 3: 15.000 - 15.000</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Asbis Referans No</label>
            <input type="text" class="form-control" name="asbis-referans-no" id="asbis" placeholder="Asbis Referans No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Ruhsat Seri No</label>
            <input type="text" class="form-control" name="ruhsat-seri-no" id="ruhsatserino" placeholder="Ruhsat Seri No">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group required">
            <label>Tercih Ettiğiniz Şirket</label>
            <input type="text" class="form-control" name="tercih-edilen-sirket" id="tercih-edilen-sirket" placeholder="Tercih Ettiğiniz Şirket">
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group required">
            <label>Eklemek İstedikleriniz</label>
            <textarea class="form-control" rows="3" name="mesaj" id="message" placeholder="Eklemek İstedikleriniz"></textarea>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="button" class="submit btn button-primary btn-lg btn-block">GÖNDER</button>
    </div>
</div>
{!! Form::close() !!}