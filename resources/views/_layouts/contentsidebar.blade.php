<nav class="secondary-sidebar padding-30">
    <ul class="main-menu">
        <li class="">
            <a class="getAltContents" data-id="banner" href="{{route('banners.index')}}">
                <span class="title"><i class=""></i>Banner</span>
            </a>
        </li>
        <li class="">
            <a class="getAltContents" data-id="generic" href="{{route('generic.index')}}">
                <span class="title"><i class=""></i>Ortak İçerikler</span>
            </a>
        </li>
        <li class="">
            <a class="getAltContents" data-id="brand" href="{{route('brands.index')}}">
                <span class="title"><i class=""></i>Şirketler</span>
            </a>
        </li>
        <li class="">
            <a class="getAltContents" data-id="forum" href="{{route('forum.index')}}">
                <span class="title"><i class=""></i>Forum</span>
            </a>
        </li>
        <li class="">
                <a class="getAltContents" data-id="insurance-packet" href="{{route('packets.index')}}">
                    <span class="title"><i class=""></i>Sigorta Paketleri</span>
                </a>
            </li>    
    </ul>
</nav>