<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
	<!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->

	<!-- END SIDEBAR MENU TOP TRAY CONTENT-->
	<!-- BEGIN SIDEBAR MENU HEADER-->
	<div class="sidebar-header">
		<img src="/cms/img/logo_white.png" alt="logo" class="brand" data-src="/cms/img/logo_white.png" data-src-retina="/cms/img/logo_white_2x.png"
		    width="39" height="40">
		<div class="sidebar-header-controls">

			<button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
				<i class="fa fs-12"></i>
			</button>
		</div>
	</div>
	<!-- END SIDEBAR MENU HEADER-->
	<!-- START SIDEBAR MENU -->
	<div class="sidebar-menu">
		<!-- BEGIN SIDEBAR MENU ITEMS-->
		<ul class="menu-items">
			<li class="m-t-30 ">
				<a href="{{route('site')}}" class="detailed">
					<span class="title">Anasayfa</span>
					<span class="details"></span>
				</a>
				<span class="bg-success icon-thumbnail">
					<i class="pg-home"></i>
				</span>
			</li>
			<li class="">
				<a href="{{route('pages.index')}}" class="detailed">
					<span class="title">Sayfalar</span>
					<span class="details"></span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-file"></i>
				</span>
			</li>
			<li>
				<a href="javascript:;">
					<span class="title">Yazılar</span>
					<span class=" arrow"></span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-file"></i>
				</span>
				<ul class="sub-menu">
					<li class="">
						<a href="{{route('posts.index')}}">Tüm Yazılar</a>
						<span class="icon-thumbnail">TY</span>
					</li>
					<li class="">
						<a href="{{route('posts.create')}}">Yeni Ekle</a>
						<span class="icon-thumbnail">Y</span>
					</li>
				</ul>
			</li>
			<li class="">
				<a href="javascript:;" class="detailed">
					<span class="title">İçerikler</span>
					<span class=" arrow"></span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-bolt"></i>
				</span>
				<ul class="sub-menu">
					<li class="">
						<a href="{{route('banners.index')}}">Banner</a>
						<span class="icon-thumbnail">B</span>
					</li>
					<li class="">
						<a href="{{route('generic.index')}}">Ortak İçerikler</a>
						<span class="icon-thumbnail">Oİ</span>
					</li>
					<li class="">
						<a href="{{route('brands.index')}}">Şirketler</a>
						<span class="icon-thumbnail">Ş</span>
					</li>
					<li class="">
						<a href="{{route('forum.index')}}">Forum</a>
						<span class="icon-thumbnail">F</span>
					</li>
					<li class="">
							<a href="{{route('packets.index')}}">Sigorta Paketleri</a>
							<span class="icon-thumbnail">SP</span>
						</li>
				</ul>
			</li>
			<li class="">
				<a href="{{route('describing.index')}}" class="detailed">
					<span class="title">Tanımlamalar</span>
					<span class="details"></span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-bolt"></i>
				</span>
			</li>
			{{--
			<li class="">
				<a href="{{route('users.index')}}">
					<span class="title">Kullanıcılar</span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-users"></i>
				</span>
			</li> --}}
			<li class="">
				<a href="javascript:;" class="detailed">
					<span class="title">Ortam</span>
					<span class=" arrow"></span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-file-image-o"></i>
				</span>
				<ul class="sub-menu">
					<li class="">
						<a href="{{route('media')}}">Galeri</a>
						<span class="icon-thumbnail">G</span>
					</li>
					<li class="">
						<a href="{{route('media-files')}}">Dosya</a>
						<span class="icon-thumbnail">D</span>
					</li>
				</ul>
			</li>
			
			<li class="">
				<a href="{{route('menu')}}">
					<span class="title">Menü</span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-bars"></i>
				</span>
			</li>
			<li class="">
				<a href="{{route('settings')}}">
					<span class="title">Ayarlar</span>
				</a>
				<span class="icon-thumbnail">
					<i class="fa fa-cogs"></i>
				</span>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->