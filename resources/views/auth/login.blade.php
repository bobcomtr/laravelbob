  @extends('_layouts.loginpage')

  @section('content')
  
  <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <img src="/cms/img/logo.png" alt="logo" data-src="/cms/img/logo.png" data-src-retina="/cms/img/logo_2x.png" width="78" height="80">
         
          <!-- START Login Form -->
         
           <form  id="form-login" class="p-t-15" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <!-- START Form Control-->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} form-group-default">
              <label>Email</label>
              <div class="controls">
               <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
              </div>
                 @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} form-group-default">
              <label>Şifre</label>
              <div class="controls">
                <input id="password" type="password" class="form-control" name="password" required>
              </div>
              @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
             @endif
            </div>
            <!-- START Form Control-->
          
            <!-- END Form Control-->
            <button class="btn btn-primary btn-cons m-t-10" type="submit">Giriş Yap</button>
          </form>
          <!--END Login Form-->       
        </div>
      </div>

@endsection