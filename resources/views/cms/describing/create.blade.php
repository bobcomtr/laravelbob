@extends('cms.app') @section('title') Yeni Kategori Ekle @endsection @section('content')
	<div class="content sm-gutter">
		<!-- START CONTAINER FLUID -->
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
				<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
				</div>
			</div>
		</div>
		<div class="container-fluid container-fixed-lg">
			<div class="">				
				<form data-js="slug" method="post" action="{{route('describing.store')}}" id="form" enctype="multipart/form-data">
					{{csrf_field()}}
					<input type="hidden" name="lang" value="tr">
					<div class="col-sm-9 no-padding">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group form-group-default form-group-default-select2 required">
									<label class="">Kategori Tipi</label>
									{!! Form::select('items', $categories, null,array('class' => 'CategoryType full-width required','data-init-plugin'=>'select2',"data-disable-search"=>"true") ) !!}
								</div>

								<div class="form-group form-group-default form-group-default-select2 required">
									<label class="">Üst Kategori</label>
									{!! Form::select('item',array(''=>''), null,array('class' => 'Category full-width required','data-init-plugin'=>'select2',"data-disable-search"=>"true",'name'=>'parent_id') ) !!}
								</div>

								<div class="form-group form-group-default required ">
									<label>Başlık</label>
									<input type="text" class="form-control" name="title" required>
								</div>
								<div class="form-group form-group-default required ">
									<label>Url</label>
									<input type="text" class="form-control" name="slug" required>
								</div>

								<div class="form-group form-group-default">
									<label>Açıklama</label>
									<textarea style="min-height:90px" rows="10" cols="10" class="form-control" name="description" required></textarea>
								</div>

								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Etiket</label>
										<input class="tagsinput custom-tag-input" type="text" name="key" value="hello World, quotes, inspiration" />
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="row">
							<div class="card share">
								<div class="card-description">
									<button class="btn btn-primary pull-right col-sm-12 col-md-5" name="status"
									        value="2"
									        type="submit">Ekle
									</button>
								</div>

							</div>
						</div>
					</div>

				</form>

			</div>
		</div>
	</div>
	</div>
@endsection
@section('js')
	<script src="/cms/js/form.js"></script>
	<script>
				{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
        var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
        $('#lfm').filemanager('image', {prefix: route_prefix});

        $('.CategoryType').trigger('change');

	</script>
@endsection