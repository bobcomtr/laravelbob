@extends('cms.app')
@section('title') Tanımlamalar @endsection
@section('secondnav')<a href="#" class="toggle-secondary-sidebar">Kategoriler<span class="text-info"></span> <span class="caret"></span></a>@endsection
@section('content')
	<div class="content full-height">
		<nav class="secondary-sidebar padding-30">
			<a href="{{route('describing.create')}}" class="btn btn-complete btn-block btn-compose m-b-30">Yeni Ekle</a>
			<ul class="main-menu">
				@foreach($categories as $category)
				<li class="">
					<a class="getAltCategories" data-id="{{$category->id}}" href="#">
						<span class="title"><i class=""></i>{{$category->title}}</span>
					</a>
				</li>
					@endforeach
			</ul>
		</nav>
		<div class="inner-content full-height">
				<div class="panel-heading" style="overflow:hidden">
					<h4 class="page-title pull-left">@yield('title')</h4>

				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
							<tr>
								<th>Başlık</th>
								<th>Actions</th>
							</tr>
							</thead>
							<tbody class="getdata">


							</tbody>
						</table>
					</div>
					{{--{{$posts->links()}}--}}
				</div>
		</div>
	</div>
@endsection
@section('css')
@endsection

@section('js')
	<script>
	$('.toggle-secondary-sidebar').click(function(e) {
	e.stopPropagation();
	$('.secondary-sidebar').toggle();
	});
    $(window).resize(function() {

        if ($(window).width() <= 1024) {
            $('.secondary-sidebar').hide();

        } else {
            $('.secondary-sidebar').show();
        }
    });
	</script>
@endsection

