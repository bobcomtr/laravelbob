<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta charset="utf-8"/>
	<title>	@yield('title')</title>
	<meta name="viewport"
	      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"
	/>
	<link rel="apple-touch-icon" href="/cms/pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/cms/ico/favicon-80x80.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/cms/ico/favicon-120x120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/cms/ico/favicon-180x180.png">
	<link rel="icon" type="image/x-icon" href="/cms/ico/favicon-80x80.png"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="LaravelBob Admin Cms" name="description"/>
	<meta content="Onur Yılmaz" name="author"/>

	<link href="/cms/css/bob.core.css" rel="stylesheet"/>
	<link href="/cms/css/app.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="/site/css/cms.css">
	@yield('css')
  <!--[if lte IE 9]>
	<link href="/cms/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen"/>
	<![endif]-->
</head>

<body class="fixed-header dashboard">
@include('_layouts.sidebar')
<!-- START PAGE-CONTAINER -->
<div class="page-container ">
	<!-- START HEADER -->
	<div class="header ">
		<!-- START MOBILE CONTROLS -->
		<div class="container-fluid relative">
			<!-- LEFT SIDE -->
			<div class="pull-left full-height visible-sm visible-xs">
				<!-- START ACTION BAR -->
				<div class="header-inner">
					<a href="#"
					   class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5"
					   data-toggle="sidebar">
						<span class="icon-set menu-hambuger"></span>
					</a>
				</div>
				<!-- END ACTION BAR -->
			</div>
			<div class="pull-center hidden-md hidden-lg">
				<div class="header-inner">
					@yield('secondnav')
					<div class="brand inline">
						<img src="/cms/img/logo.png" alt="logo" data-src="/cms/img/logo.png"
						     data-src-retina="/cms/img/logo_2x.png" width="39" height="40">
					</div>
				</div>
			</div>
			<!-- RIGHT SIDE -->

		</div>
		<!-- END MOBILE CONTROLS -->
		<div class=" pull-left sm-table hidden-xs hidden-sm">
			<div class="header-inner">
				<div class="brand inline">
					<img src="/cms/img/logo.png" alt="logo" data-src="/cms/img/logo.png"
					     data-src-retina="/cms/img/logo_2x.png" width="39" height="40">
				</div>

			</div>
		</div>
		<div class=" pull-right">
			<!-- START User Info-->
			<div class="visible-lg visible-md m-t-10">
				<div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
					<span class="semi-bold">{{Auth::user()->name}}</span>
				</div>
				<div class="dropdown pull-right">
					<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
					        aria-expanded="false">
              <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="/cms/img/profiles/avatar.jpg" alt="" data-src="/cms/img/profiles/avatar.jpg"
                     data-src-retina="/cms/img/profiles/avatar_small2x.jpg"
                     width="32" height="32">
              </span>
					</button>
					<ul class="dropdown-menu profile-dropdown" role="menu">
						<li>
							<a target="_blank" href="{{route('home')}}">
								<i class="pg-home"></i>Siteye Git</a>
						</li>
						<li class="bg-master-lighter">
							<a href="{{ route('logout') }}" class="clearfix"
							   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<span class="pull-left">Çıkış Yap</span>
								<span class="pull-right">
                    <i class="pg-power"></i>
                  </span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- END User Info-->
		</div>
	</div>

	<!-- END HEADER -->
	<!-- START PAGE CONTENT WRAPPER -->
	<div class="page-content-wrapper ">
		<!-- START PAGE CONTENT -->

	@yield('content')


	<!-- START COPYRIGHT -->
		<div class="container-fluid container-fixed-lg footer">
			<div class="copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright &copy; 2018 </span>
					<span class="font-montserrat">
              <a target="_blank" href="http://www.bob.com.tr">Bob.com.tr</a>
            </span>
				</p>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END COPYRIGHT -->


	</div>
	<!-- END PAGE CONTAINER -->

	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>

	<script src="/cms/js/pace.js"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>

	@yield('js')


	<script src="/cms/js/bob.core.js"></script>

	<script src="/cms/js/app.js"></script>


	<!-- END CORE TEMPLATE JS -->
	<!-- BEGIN PAGE LEVEL JS -->


</body>

</html>