@extends('cms.app') 
@section('title') Ortak İçerik Düzenle
@endsection
 
@section('content')
<div class="content sm-gutter">
    <!-- START CONTAINER FLUID -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h4 class="page-title">@yield('title')</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <form data-js="summernote" action="{{route('generic.update',$generic->id)}}" method="POST" id="form" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('PATCH') }}
                <div class="col-sm-8 col-md-9 no-padding">
                    <input type="hidden" name="lang" value="tr">
                    <input type="hidden" name="type" value="generic"> @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                            <label>Başlık</label>
                            <input type="text" class="form-control" readonly name="title" required value="{{$generic->title}}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                            <label>Kısa Kod</label>
                            <input type="text" class="form-control required" readonly name="slug" placeholder="" required value="{{$generic->slug}}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <h5>İçerik</h5>
                        <div class="summernote-wrapper required">
                            <textarea id="summernote-editor" class="required" name="content">{{$generic->content}}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Açıklama</label>
                            <textarea style="min-height:90px" rows="10" cols="10" class="form-control" name="excerpt">{{$generic->excerpt}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="row">
                        <div class="card share">
                            <div class="card-header clearfix">
                                <div class="user-pic">
                                    <img alt="Profile Image" width="33" height="33" data-src-retina="/cms/img/profiles/avatar.jpg" data-src="/cms/img/profiles/avatar.jpg"
                                        src="/cms/img/profiles/avatar.jpg">
                                </div>
                                <h5>{{$generic->user->name}}</h5>
                                <h6>{{__('cms.'.$generic->status)}}
                                    <span class="location semi-bold">
                                        <i class="fa fa-clock-o"></i> {{$generic->published_at}}</span>
                                </h6>
                            </div>
                            <div class="card-description">
                                @if (Auth::user()->hasRole('superadministrator'))
                                <a class="delete-btn btn btn-danger pull-left" style="margin-right:10px;" href="{{ URL::route('generic.destroy',$generic->id) }}">Sil</a>                                
                                <button class="btn btn-warning pull-left" type="submit" name="status" value="draft">Taslak</button>@endif
                                <button type="submit" class="btn btn-primary pull-right" name="status" value="published">Düzenle</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
 
@section('css')
@endsection
 
@section('js')
<script src="/cms/js/form.js"></script>

<script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
            var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
            $('#lfm').filemanager('image', {prefix: route_prefix});
</script>
@endsection