 @extends('cms.app') @section('title') Ortak İçerik Ekle @endsection @section('content')
<div class="content sm-gutter">
	<!-- START CONTAINER FLUID -->
	<div class="jumbotron" data-pages="parallax">
		<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg">
		<div class="row">
			<form data-js="slug summernote" action="{{route('generic.store')}}" method="POST" id="form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-8 col-md-9 no-padding">
						<input type="hidden" name="lang" value="tr">
				<input type="hidden" name="type" value="generic">
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Başlık</label>
							<input type="text" class="form-control" name="title" required value="">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group form-group-default input-group required">
						<span class="input-group-addon">
							<label>Kısakod:</label>
						</span>
							<label>Url</label>
							<input type="text" class="form-control" name="slug" placeholder="" required
								   value="">
						</div>
					</div>
					<div class="col-sm-12">
						<h5>İçerik</h5>
						<div class="summernote-wrapper">
							<textarea id="summernote-editor" name="content"></textarea>

						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Açıklama</label>
							<textarea style="min-height:90px" rows="10" cols="10" class="form-control required" name="excerpt"></textarea>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="card share">							
							<div class="card-description">
								@if (Auth::user()->hasRole('superadministrator'))<button class="btn btn-warning pull-left" type="submit" name="status" value="draft">Taslak</button>@endif
								<button type="submit" class="btn btn-primary pull-right" name="status" value="published">Yayınla</button>

							</div>

						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection @section('css') @section('js')
<script src="/cms/js/form.js"></script>

<script>
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    $('#lfm').filemanager('image', {prefix: route_prefix});

</script>
@endsection