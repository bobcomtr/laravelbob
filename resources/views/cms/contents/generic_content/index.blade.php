@extends('cms.app')
@section('title') Ortak İçerikler @endsection
@section('secondnav')<a href="#" class="toggle-secondary-sidebar">İçerikler<span class="text-info"></span> <span class="caret"></span></a>@endsection

@section('content')
    <div class="content full-height">
        @include('_layouts.contentsidebar')
        <div class="inner-content full-height">
            <div class="jumbotron" data-pages="parallax">
                <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                    <div class="inner" style="transform: translateY(0px); opacity: 1;">
                        <h4 class="page-title">@yield('title')</h4>
                    </div>
                    @if (Auth::user()->hasRole('superadministrator'))  
                    <div class="new-append-button ">
                            <a href="{{route('generic.create')}}" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> <span>Yeni Ekle</span>
                                </a>
                        </div>
                        @endif
                </div>
            </div>            
            <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block dataTable no-footer reorder" id="datatable" width="100%">
                        <thead>
                        <tr>
                            <th>Başlık</th>                            
                            <th>Tarih</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
           </div>
        </div>
    </div>
@endsection
@section('css')

@endsection

@section('js')
    <script src="/cms/js/datatable.js"></script>
    <script>
        columns= [
            { data: 'title', name: 'title' },          
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        setDatatable('{!! route('generic.datatable') !!}',columns,false,"App\\Content");
    </script>
    <script>
            $('.toggle-secondary-sidebar').click(function(e) {
            e.stopPropagation();
            $('.secondary-sidebar').toggle();
            });
            $(window).resize(function() {
        
                if ($(window).width() <= 1024) {
                    $('.secondary-sidebar').hide();
        
                } else {
                    $('.secondary-sidebar').show();
                }
            });
            </script>
@endsection