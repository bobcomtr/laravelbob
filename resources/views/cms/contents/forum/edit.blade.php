@extends('cms.app') @section('title') Forum İçerik Düzenle @endsection @section('content')

<div class="content sm-gutter">
		<!-- START CONTAINER FLUID -->
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
				<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
				</div>
			</div>
		</div>
		<div class="container-fluid container-fixed-lg">
				<div class="row">
	<form data-js="slug summernote" action="{{route('forum.update',$forum->id)}}" method="post" id="form" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<input type="hidden" name="cat-id" value='@isset($details["category"]) $details["category"] @endisset '>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
		<div class="col-sm-8 col-md-9 no-padding">
			<ul class="nav  nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
				<li class="active">
					<a data-toggle="tab" href="#slide1">
						<span>Genel</span>
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#slide2">
						<span>Seo</span>
					</a>
				</li>				
			</ul>
			<div class="tab-content">
				<div class="tab-pane slide-left active" id="slide1">
					<div class="row column-seperation">
						<div class="col-sm-6">
							<div class="form-group form-group-default required">
								<label>Başlık</label>
								<input type="text" class="form-control required" name="title" required value="{{$forum->title}}">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group form-group-default input-group required">
                            <span class="input-group-addon">
                                http://laravel.bob/
                            </span>
								<label>Url</label>
								<input type="text" class="form-control required" name="slug" placeholder="" required
								       value="{{$forum->slug}}">
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group form-group-default required">
								<label>Özet</label>
								<textarea style="min-height:90px" rows="10" cols="10" class="form-control required"
								          name="excerpt">{{$forum->excerpt}}</textarea>
							</div>
						</div>
						
						<div class="col-sm-12 required">
							<h5>İçerik</h5>
							<div class="summernote-wrapper required">
								<textarea id="summernote-editor" name="content">{{$forum->content}}</textarea>

							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane slide-left" id="slide2">
					<div class="row column-seperation">
						<div class="col-sm-12">
							<div class="form-group form-group-default required">
								<label>Meta Title</label>
								<input type="text" class="form-control" name="detail[metatit]" value="@isset($details['metatit']){{$details['metatit']}} @endisset">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group form-group-default required">
								<label>Meta Key</label>
								<input type="text" class="form-control" name="detail[metakey]" value="@isset($details['metakey']){{$details['metakey']}} @endisset">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group form-group-default required">
								<label>Meta Description</label>
								<input type="text" class="form-control" name="detail[metadesc]" value="@isset($details['metadesc']){{$details['metadesc']}} @endisset">
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="row">
				<div class="card share">
					<div class="card-header clearfix">
						<div class="user-pic">
							<img alt="Profile Image" width="33" height="33"
							     data-src-retina="/cms/img/profiles/avatar.jpg" data-src="/cms/img/profiles/avatar.jpg"
							     src="/cms/img/profiles/avatar.jpg">
						</div>
                        <h5>{{$forum->user->name}}</h5>
                        <h6>{{__('cms.'.$forum->status)}}
                            <span class="location semi-bold">
                            <i class="fa fa-clock-o"></i> {{$forum->published_at}}</span>
                        </h6>
					</div>
					<div class="card-description">
                        <a class="delete-btn btn btn-danger pull-left" style="margin-right:10px;"  href="{{ URL::route('forum.destroy',$forum->id) }}">Sil</a>
                        <button class="btn btn-warning pull-left" type="submit" name="status" value="draft">Taslak</button>
                        <button type="submit" class="btn btn-primary pull-right" name="status" value="published">Düzenle</button>
					</div>
				</div>
			</div>
		</div>
	</form>

</div>
</div>
</div>
</div>
@endsection @section('css')
@section('js')
	<script src="/cms/js/form.js"></script>

	<script>
				{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
        var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
		$('#lfm').filemanager('image', {prefix: route_prefix});
		
	</script>
@endsection