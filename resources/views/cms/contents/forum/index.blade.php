@extends('cms.app') 
@section('title') Forum İçerikleri @endsection 
@section('secondnav')<a href="#" class="toggle-secondary-sidebar">İçerikler<span class="text-info"></span> <span class="caret"></span></a>@endsection

@section('content')
<div class="content full-height">
    @include('_layouts.contentsidebar')
    <div class="inner-content full-height">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner" style="transform: translateY(0px); opacity: 1;">
                    <h4 class="page-title">@yield('title')</h4>
                </div>
                <div class="new-append-button ">
                    <a href="{{route('forum.create')}}" class="btn btn-primary">
                            <i class="fa fa-plus"></i> <span>Yeni Ekle</span>
                        </a>
                </div>
            </div>
        </div>
        <div class="panel-body">            
            <table class="table table-hover demo-table-search table-responsive-block dataTable no-footer reorder" id="datatable" width="100%">
                <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Durum</th>
                        <th>Sıralama</th>
                        <th>Tarih</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('css')

@endsection

@section('js')
    <script src="/cms/js/datatable.js"></script>
    <script>
        columns= [
            { data: 'title', name: 'title' },
            { data: 'status', name: 'status' },
            { data: 'rank', name: 'rank' },
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        setDatatable('{!! route('forum.datatable') !!}',columns,true,"App\\Content");
    </script>
    <script>
            $('.toggle-secondary-sidebar').click(function(e) {
            e.stopPropagation();
            $('.secondary-sidebar').toggle();
            });
            $(window).resize(function() {
        
                if ($(window).width() <= 1024) {
                    $('.secondary-sidebar').hide();
        
                } else {
                    $('.secondary-sidebar').show();
                }
            });
            </script>
@endsection