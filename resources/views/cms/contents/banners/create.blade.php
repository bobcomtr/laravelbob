 @extends('cms.app') @section('title') Banner Ekle @endsection @section('content')
<div class="content sm-gutter">
	<!-- START CONTAINER FLUID -->
	<div class="jumbotron" data-pages="parallax">
		<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg">
		<div class="row">
			<form data-js="summernote" action="{{route('banners.store')}}" method="POST" id="form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-8 col-md-9 no-padding">
					<input type="hidden" name="type" value="banner"> @if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Başlık</label>
							<input type="text" class="form-control" name="title" required value="">
						</div>
					</div>
					<div class="col-sm-12">                           
							<h5>Banner Başlık</h5>
						<div class="summernote-wrapper required">
						<textarea style="min-height:90px" rows="10" cols="10" id="summernote-editor" name="excerpt"></textarea>
				</div>
				</div>
					<div class="col-sm-12">
							<div class="form-group form-group-default">
						<label>Banner Açıklama</label>
							<textarea class="form-control" name="content"> </textarea>
							</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group form-group-default">
							<label>Button Url</label>
							<input type="text" class="form-control" name="detail[buttonurl]" value="">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Button Text</label>
							<input type="text" class="form-control" name="detail[buttontext]" required value="">
						</div>
					</div>

					<div class="col-sm-12">
						<div class="input-group form-group-default required">
							<span class="input-group-btn">
								<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
									<i class="fa fa-picture-o"></i> (1400px X 500px) Görsel Seç
								</a>
							</span>

							<input id="thumbnail" class="form-control" value="" type="text" name="detail[image]">
						</div>
						<img id="holder" style="margin-top:15px;max-height:100px;" src="">
					</div>
				</div>
				<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="card share">
								<div class="card-description">
	
									<button class="btn btn-warning pull-left col-sm-12 col-md-5 m-b-10" name="status" value="draft" type="submit">Taslak
									</button>
									<button class="btn btn-primary pull-right col-sm-12 col-md-5" name="status"  value="published" type="submit">Yayınla
									</button>
								</div>
							</div>
						</div>
					</div>
			</form>

		</div>
	</div>
</div>
@endsection @section('css') @section('js')
<script src="/cms/js/form.js"></script>

<script>
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    $('#lfm').filemanager('image', {prefix: route_prefix});

</script>
@endsection