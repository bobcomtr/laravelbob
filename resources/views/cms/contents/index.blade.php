@extends('cms.app')
@section('title') İçerikler @endsection
@section('secondnav')<a href="#" class="toggle-secondary-sidebar">İçerikler<span class="text-info"></span> <span class="caret"></span></a>@endsection
@section('content')
	<div class="content full-height">
		@include('_layouts.contentsidebar')
		<div class="inner-content full-height">
				<div class="panel-heading" style="overflow:hidden">
					<h1 class="page-title pull-left">@yield('title')</h1>

				</div>
				<div class="panel-body">

				</div>
		</div>
	</div>
@endsection
@section('css')
@endsection

@section('js')
	<script>
	$('.toggle-secondary-sidebar').click(function(e) {
	e.stopPropagation();
	$('.secondary-sidebar').toggle();
	});
    $(window).resize(function() {

        if ($(window).width() <= 1024) {
            $('.secondary-sidebar').hide();

        } else {
            $('.secondary-sidebar').show();
        }
    });
	</script>
@endsection

