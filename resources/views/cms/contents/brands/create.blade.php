 @extends('cms.app') @section('title') Şirket Ekle @endsection @section('content')
<div class="content sm-gutter">
	<!-- START CONTAINER FLUID -->
	<div class="jumbotron" data-pages="parallax">
		<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg">
		<div class="row">
			<form data-js="summernote" action="{{route('brands.store')}}" method="POST" id="form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-sm-8 col-md-9 no-padding">
					<input type="hidden" name="type" value="brand"> @if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Başlık</label>
							<input type="text" class="form-control" name="title" required value="">
						</div>
					</div>
					<div class="col-sm-12 hide">                           
						<h5>Şirket Başlık</h5>
						<div class="summernote-wrapper">
							<input type="text" class="form-control" name="excerpt" value="-">
						</div>
				</div>
					<div class="col-sm-12">
							<div class="form-group form-group-default required">
						<label>Şirket Açıklama</label>
						<input type="text" class="form-control" name="content" required value="">
							</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group form-group-default required">
							<label>Logo Url</label>
							<input type="text" class="form-control" name="detail[buttonurl]" required value="">
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="input-group">
							<span class="input-group-btn">
								<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
									<i class="fa fa-picture-o"></i> (228px X 72px) Görsel Seç
								</a>
							</span>

							<input id="thumbnail" class="form-control" value="" type="text" name="detail[image]">
						</div>
						<img id="holder" style="margin-top:15px;max-height:100px;" src="">
					</div>
				</div>

				<div class="col-sm-4 col-md-3 m-t-50 sm-m-t-0">
						<div class="row">
							<div class="card share">
								<div class="card-description">
	
									<button class="btn btn-warning pull-left col-sm-12 col-md-5 m-b-10" name="status" value="draft" type="submit">Taslak
									</button>
									<button class="btn btn-primary pull-right col-sm-12 col-md-5" name="status"  value="published" type="submit">Yayınla
									</button>
								</div>
							</div>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>
@endsection @section('css') @section('js')
<script src="/cms/js/form.js"></script>

<script>
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    $('#lfm').filemanager('image', {prefix: route_prefix});

</script>
@endsection