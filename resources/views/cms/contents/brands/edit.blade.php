 @extends('cms.app') @section('title') Şirket Düzenle @endsection @section('content')
<div class="content sm-gutter">
    <!-- START CONTAINER FLUID -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h4 class="page-title">@yield('title')</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <form data-js="slug summernote" action="{{route('brands.update',$brand->id)}}" method="POST" id="form" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('PATCH') }}
                <div class="col-sm-9 no-padding">
                    <input type="hidden" name="type" value="brand"> @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                            <label>Başlık</label>
                            <input type="text" class="form-control" name="title" required value="{{$brand->title}}">
                        </div>
                    </div>
                    <div class="col-sm-12 hide">
                        <h5>Şirket Başlık</h5>
                        <div class="summernote-wrapper">
                            <textarea style="min-height:90px" rows="10" cols="10" id="summernote-editor" name="excerpt">{{$brand->excerpt}}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Şirket Açıklama</label>
                            <input type="text" class="form-control" name="content" required value="{{$brand->content}}">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                            <label>Button Url</label>
                            <input type="text" class="form-control" name="detail[buttonurl]" required value="{{isset($details['buttonurl'])?$details['buttonurl']:''}}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i>(228px X 72px) Görsel Seç
                                </a>
                            </span>
                            <input id="thumbnail" class="form-control" value="{{isset($details['image'])?$details['image']:''}}"
                                type="text" name="detail[image]">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;" src="{{isset($details['image'])?$details['image']:''}}">
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="row">
                        <div class="card share">
                            <div class="card-header clearfix">
                                <div class="user-pic">
                                    <img alt="Profile Image" width="33" height="33" data-src-retina="/cms/img/profiles/avatar.jpg" data-src="/cms/img/profiles/avatar.jpg"
                                        src="/cms/img/profiles/avatar.jpg">
                                </div>
                                <h5>{{$brand->user->name}}</h5>
                                <h6>{{__('cms.'.$brand->status)}}
                                    <span class="location semi-bold">
                                        <i class="fa fa-clock-o"></i> {{$brand->published_at}}</span>
                                </h6>
                            </div>
                            <div class="card-description">
                                <a class="delete-btn btn btn-danger pull-left" style="margin-right:10px;" href="{{ URL::route('brands.destroy',$brand->id) }}">Sil</a>
                                <button class="btn btn-warning pull-left" type="submit" name="status" value="draft">Taslak</button>
                                <button type="submit" class="btn btn-primary pull-right" name="status" value="published">Düzenle</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection @section('css') @section('js')
<script src="/cms/js/form.js"></script>

<script>
    {!!\File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    $('#lfm').filemanager('image', {
        prefix: route_prefix
    });
</script>
@endsection