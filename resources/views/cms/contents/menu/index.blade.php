@extends('cms.app') 
@section('title') Menü Düzenle
@endsection
 
@section('content')
<div class="content sm-gutter">
    <!-- START CONTAINER FLUID -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h4 class="page-title">@yield('title')</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body" id="cont">
                        <ul id="myEditor" class="sortableLists list-group">
                        </ul>
                    </div>
                </div>
                <form method="post" id="form">
                    {{csrf_field()}}
                    <input type="hidden" name="lang" value="tr">
                    <div class="form-group">
                        <button class="btn btn-primary  col-sm-12 col-md-5" name="status" value="2" type="submit">Menüyü Kaydet
                    </button>
                    </div>
                    <div class="form-group hide"><textarea id="out" name="json" class="form-control" cols="50" rows="10"></textarea>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="frmEdit" class="form-horizontal">
                            <div class="form-group">
                                <label for="text" class="col-sm-2 control-label">Başlık</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control item-menu required" name="text" id="text" required placeholder="Başlık">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="href" class="col-sm-2 control-label">URL</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control item-menu required" id="href" name="href" required placeholder="URL">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="target" class="col-sm-2 control-label">Target</label>
                                <div class="col-sm-10">
                                    <select name="target" id="target" class="form-control item-menu">
                                        <option value="_self">Self</option>
                                        <option value="_blank">Blank</option>
                                        <option value="_top">Top</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnReload" type="button" class="btn btn-default hide">
                        <i class="glyphicon glyphicon-triangle-right"></i> Load Data</button>
                    <div class="panel-footer">
                        <button type="button" id="btnUpdate" class="btn btn-primary" style="display:none" disabled><i class="fa fa-refresh"></i> Güncelle</button>
                        <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Ekle</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
 
@section('css')
<style>
    span.txt {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 150px;
        display: inline-block;
    }
</style>
@endsection
 
@section('js')
<script src="/cms/js/form.js"></script>
<script>
    jQuery(document).ready(function () {
        var strjson =  {!! $menu !!};
        var options = {
            opener: {
                as: 'html',
                close: '<i class="fa fa-minus"></i>',
                open: '<i class="fa fa-plus"></i>',
                openerCss: {'margin-right': '10px'},
                openerClass: 'btn btn-success btn-xs'
            }
        };
        
        var editor = new MenuEditor('myEditor', {listOptions: options,  labelEdit: 'Düzenle'});
        editor.setForm($('#frmEdit'));
        editor.setUpdateButton($('#btnUpdate'));
        editor.setData(strjson);
        var str = editor.getString();
        $("#out").text(str);
        $('#btnReload').on('click', function () {
            var str = editor.getString();
            $("#out").text(str);
        });
        var str = editor.getString();     
        $("#btnUpdate").click(function(){
            editor.update();
            var str = editor.getString();
            $("#out").text(str);
            $("#btnUpdate").hide();
            $("#btnAdd").show();
        });
        $('#btnAdd').click(function(){
            editor.add();
            var str = editor.getString();
            $("#out").text(str);
        });
    });
</script>
@endsection