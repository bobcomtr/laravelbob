@extends('cms.app') 
@section('title') Sayfa Düzenle
@endsection
 
@section('content')
<div class="content sm-gutter">
	<!-- START CONTAINER FLUID -->
	<div class="jumbotron" data-pages="parallax">
		<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg">
		<div class="row">
			<form data-js="slug summernote" action="{{route('pages.update',$post->id)}}" method="POST" id="form" enctype="multipart/form-data">
				{{ csrf_field() }} {{ method_field('PATCH') }}
				<div class="col-sm-8 col-md-9 no-padding">
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<ul class="nav  nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
						<li class="active">
							<a data-toggle="tab" href="#slide1">
						<span>Genel</span>
					</a>
						</li>
						<li>
							<a data-toggle="tab" href="#slide2">
						<span>Seo</span>
					</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane  active" id="slide1">
							<div class="row column-seperation">
								<div class="col-sm-6">
									<div class="form-group form-group-default required">
										<label>Başlık</label>
										<input type="text" class="form-control" name="title" required value="{{$post->title}}">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group form-group-default input-group">
										<span class="input-group-addon">
                                http://laravel.bob/
                            </span>
										<label>Url</label>
										<input type="text" class="form-control" name="slug" placeholder="" required value="{{$post->slug}}">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default">
										<label>Özet</label>
										<textarea style="min-height:90px" rows="10" cols="10" class="form-control" name="excerpt">{{$post->excerpt}}</textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<h5>İçerik</h5>
									<div class="summernote-wrapper">
										<textarea id="summernote-editor" name="content">{{$post->content}}</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane slide-left" id="slide2">
							<div class="row column-seperation">
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Title</label>
										<input type="text" class="form-control" name="detail[metatit]" value="@isset($details['metatit']){{$details['metatit']}} @endisset">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Key</label>
										<input type="text" class="form-control" name="detail[metakey]" value="@isset($details['metakey']){{$details['metakey']}} @endisset">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Description</label>
										<input type="text" class="form-control" name="detail[metadesc]" value="@isset($details['metadesc']){{$details['metadesc']}} @endisset">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="card share">
							<div class="card-header clearfix">
								<div class="user-pic">
									<img alt="Profile Image" width="33" height="33" data-src-retina="/cms/img/profiles/avatar.jpg" data-src="/cms/img/profiles/avatar.jpg"
									 src="/cms/img/profiles/avatar.jpg">
								</div>
								<h5>{{$post->user->name}}</h5>
								<h6>{{__('cms.'.$post->status)}}
									<span class="location semi-bold">
								<i class="fa fa-clock-o"></i> {{$post->published_at}}</span>
								</h6>
							</div>
							<div class="card-description">
								<a class="delete-btn btn btn-danger pull-left" style="margin-right:10px;" href="{{ URL::route('pages.destroy',$post->id) }}">Sil</a>
								<button class="btn btn-warning pull-left" type="submit" name="status" value="draft">Taslak</button>
								<button type="submit" class="btn btn-primary pull-right" name="status" value="published">Düzenle</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
@endsection
 
@section('css') 
@section('js')
<script src="/cms/js/form.js"></script>
<script>
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
        var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
		$('#lfm').filemanager('image', {prefix: route_prefix});
		
		$(function(){
		
			$.get("/api/category/1", function (data) {
				if (data.length > 0) {
					var compiled = _.template(
						'<% _.forEach(categories, function(category) { %><option value="<%- category.id %>"><%- category.title %></option><% }); %>'
					);
					$("select.Category").empty().trigger("select2.change");
					$('select.Category').append(compiled({
						'categories': data
					})).trigger('change');
					$selected_id=$("input[name='cat-id']").val();					;
					$('select.Category').select2('val',$selected_id).trigger('change');
				}
			})
		})
</script>
@endsection