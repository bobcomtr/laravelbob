@extends('cms.app') @section('title') Sayfa Yazı Ekle @endsection @section('content')
<div class="content sm-gutter">
	<!-- START CONTAINER FLUID -->
	<div class="jumbotron" data-pages="parallax">
		<div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<h4 class="page-title">@yield('title')</h4>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg">
		<div class="row">
			<form data-js="slug summernote" method="post" action="{{route('pages.store')}}" id="form" enctype="multipart/form-data">
				{{csrf_field()}}
				<input type="hidden" name="lang" value="tr">
				<input type="hidden" name="type" value="page">
				<input type="hidden" name="parent_id" value="<?=$_GET['parentid'];?>">
				<div class="col-sm-9 no-padding">
					<ul class="nav  nav-tabs nav-tabs-fillup">
						<li class="active">
							<a data-toggle="tab" href="#slide1">
								<span>Genel</span>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#slide2">
								<span>Seo</span>
							</a>
						</li>					
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="slide1">
							<div class="row column-seperation">
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Başlık</label>
										<input type="text" class="form-control required" name="title" required>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>{{ config('settings.siteurl') }}</label>
										<input tabindex="-1" type="text" class="form-control required" name="slug" placeholder="" required>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Özet</label>
										<textarea style="min-height:90px" rows="10" cols="10" class="form-control required" name="excerpt" required> </textarea>
									</div>
								</div>							
								
								<div class="col-sm-12">
										<div class="form-group form-group-default required">
									<h5>İçerik</h5>
									<div class="summernote-wrapper">
										<textarea id="summernote-editor" class="required" name="content" required></textarea>
									</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="tab-pane slide-left" id="slide2">
							<div class="row column-seperation">
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Title</label>
										<input type="text" class="form-control" name="detail[metatit]">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Key</label>
										<input type="text" class="form-control" name="detail[metakey]">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Meta Description</label>
										<input type="text" class="form-control" name="detail[metadesc]">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-3 m-t-50 sm-m-t-0">
					<div class="row">

						<div class="card share">
							<div class="card-description">

								<button class="btn btn-warning pull-left col-sm-12 col-md-5 m-b-10" name="status" value="draft" type="submit">Taslak
								</button>
								<button class="btn btn-primary pull-right col-sm-12 col-md-5" name="status" value="published" type="submit">Yayınla
								</button>
							</div>

						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
</div>
@endsection 
@section('js')
<script src="/cms/js/form.js"></script>

<script>
		
	{!!\File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
	var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
	$('#lfm').filemanager('image', {
		prefix: route_prefix
	});
	$(function(){
		
		$.get("/api/category/1", function (data) {
			if (data.length > 0) {
				var compiled = _.template(
					'<% _.forEach(categories, function(category) { %><option value="<%- category.id %>"><%- category.title %></option><% }); %>'
				);
				$("select.Category").empty().trigger("select2.change");
				$('select.Category').append(compiled({
					'categories': data
				})).trigger('change');
	
			}
		})
	})
</script>
@endsection