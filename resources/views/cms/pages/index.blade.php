@extends('cms.app')
@section('title') Sayfalar @endsection

@section('content')
	<div class="content sm-gutter">

		<div class="container-fluid container-fixed-lg bg-white">
			<!-- START PANEL -->
			<div class="panel panel-transparent">
				<div class="panel-heading">
					<div class="panel-title">@yield('title')
					</div>									
				</div>
				<div class="panel-body">
					<div class="pagelist col-lg-3 col-md-4 col-sm-4 col-xs-12 first">
					<div class="pagelist-header">
						<a class="pull-left addPage" data-id="0" href="/manage/pages/create?parentid=0&crm=2q90rmc2r294352v90mtu029cm924u93"><i class="fa fa-plus"></i></a>
						<h4>Anasayfa</h4>
					</div>
						<ul>
							@foreach($page as $p)
							<li><span class="link context-menu" data-id="{{$p->id}}"><i class="fa {{$p->status}}"></i>{{$p->title}}</span>
							<a href="/manage/pages/{{$p->id}}/edit" title="Düzenle"><i class="fa fa-pencil"></i></a>
							@endforeach
						</ul>
					</div>
					<div class="other"></div>
					
				</div>
				
			</div>
			<!-- END PANEL -->
		</div>
		<!-- START CONTAINER FLUID -->
	</div>

@endsection
@section('css')

@endsection

@section('js')
<script type="text/template" id="pagelist-Template">
	<div class="pagelist col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<div class="pagelist-header">
			<a class="pull-left addPage" data-id="0" href="#"><i class="fa fa-plus"></i></a>
			<h4 class="title">Anasayfa</h4>
		</div>
		<ul>
		<% _.forEach(pages, function(page) { %>
		<li>
			<span class="link context-menu" data-id="<%- page.id %>"><i class="fa <%- page.status %>"></i> <%- page.title %></span>
			<a href="/manage/pages/<%- page.id %>/edit" title="Düzenle"><i class="fa fa-pencil"></i></a>
		</li>
		<% }); %>
		</ul>
	</div>
</script>


@endsection

