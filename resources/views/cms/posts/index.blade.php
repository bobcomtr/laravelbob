@extends('cms.app')
@section('title') Yazılar @endsection

@section('content')
	<div class="content sm-gutter">

		<div class="container-fluid container-fixed-lg bg-white">
			<!-- START PANEL -->
			<div class="panel panel-transparent">
				<div class="panel-heading">
					<div class="panel-title">Yazılar
					</div>					
					<div class="pull-right">
						<div class="row">
						<div class="col-xs-12">
							<a href="{{route('posts.create')}}" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Yeni
								Ekle</a>
						</div>
						</div>
					</div>

					<div class="export-options-container pull-right"></div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body">
					<table class="table table-hover demo-table-search table-responsive-block dataTable no-footer reorder" id="datatable" width="100%">
					<thead>
					<tr>
						
						<th></th>
						<th>url</th>
						<th>url</th>
						<th>Durum</th>
						<th>Sıralama</th>
						<th>Tarih</th>
						<th>Düzenleme</th>
						<th></th>
					</tr>
				</thead>
					</table>
				</div>
			</div>
			<!-- END PANEL -->
		</div>
		<!-- START CONTAINER FLUID -->
	</div>

@endsection
@section('css')

@endsection

@section('js')
  	<script src="/cms/js/datatable.js"></script>
	<script>
	columns= [			
			{data: 'render', name: 'render', orderable: false, searchable: false},
			{ data: 'title', name: 'title' },
			{ data: 'slug', name: 'slug' },		
			{ data: 'status', name: 'status' },
			{ data: 'rank', name: 'rank' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
			{data: 'action', name: 'action', orderable: false, searchable: false}

    ]
	setDatatable('{!! route('posts.datatable') !!}',columns,true,"App\\Post");
	</script>
@endsection