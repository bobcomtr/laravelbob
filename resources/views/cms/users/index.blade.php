@extends('cms.app')
@section('title') Kullanıcılar @endsection
@section('topbutton') <a href="{{route('users.create')}}" class="btn m-t-10 btn-primary btn-cons pull-right">Yeni Ekle</a> @endsection

@section('content')
<div class="panel-body">
<div class="table-responsive">
<table class="table table-striped">
<thead>
<tr>
    <th>id</th>
    <th>Ad</th>
    <th>Email</th>
    <th>Eklenme Tarihi</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>
@foreach($users as $user)

<tr>
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->created_at->formatLocalized('%d %B %Y')}}</td>
    <td><a href="{{route('users.edit',$user->id)}}" class="btn btn-warning">Düzenle</a></td>
</tr>
@endforeach
</tbody>
</table>
</div>
{{$users->links()}}
@endsection
@section('css')
@endsection

@section('js')
@endsection

