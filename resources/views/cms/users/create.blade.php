@extends('cms.app')
@section('title') Kullanıcı Ekle @endsection
@section('content')
<form action="{{route('users.store')}} method='POST'" class="form-horizontal form-label-left"> 
{{csrf_field()}}
 {{ Form::bsText('name','Adı') }}
 {{ Form::bsText('email','Email') }}
 {{ Form::bsText('password','password') }}
 <b-checkbox name='auto_genarate' :checked="true">Auto Generate Password</b-checkbox>
   <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">                           
        <button type="submit" class="btn btn-success">Kaydet</button>
      </div>
    </div>
</form>
@endsection
@section('css')
@endsection

@section('js')
@endsection

