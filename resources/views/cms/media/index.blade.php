@extends('cms.app')
@section('title') Sayfalar @endsection

@section('content')
    <iframe src="/laravel-filemanager?type=image" style="width: 100%; height: 600px; overflow: hidden; border: none;"></iframe>

@endsection
@section('css')

@endsection

@section('js')
    <script type="text/template" id="pagelist-Template">
        <div class="pagelist col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="pagelist-header">
                <a class="pull-left addPage"  data-id="" href="#"><i class="fa fa-plus"></i></a>
                <h4 class="title">Anasayfa</h4>
            </div>
            <ul>
                <% _.forEach(pages, function(page) { %>
                <li>
                    <a href="javascript:;" data-id="<%- page.id %>"><i class="fa <%- page.status %>"></i> <%- page.title %></a>
                </li>
                <% }); %>
            </ul>
        </div>
    </script>
@endsection

