@extends('cms.app') 
@section('title') Site Ayarları
@endsection
 
@section('content')
<div class="content sm-gutter">
    <!-- START CONTAINER FLUID -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h4 class="page-title">@yield('title')</h4>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <form method="post" data-js="summernote" id="form" class="form-horizontal form-label-left" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav  nav-tabs nav-tabs-fillup" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#genel" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Genel Ayarlar</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#iletisim" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">İletişim</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#sosyal" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Sosyal</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#google" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Google Api</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#modal" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Modal Ayarları</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#mailsettings" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Mail</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="genel" aria-labelledby="home-tab">
                                {{ Form::bsText('sitename','Site Başlığı',@$Settings['sitename']) }} {{ Form::bsText('meta_keyword','Keyword',@$Settings['meta_keyword'])
                                }} {{ Form::bsText('meta_desc','Description',@$Settings['meta_desc']) }} {{ Form::bsText('siteurl','Site
                                Url',@$Settings['siteurl']) }}
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="iletisim" aria-labelledby="profile-tab">
                                {{ Form::bsText('adres','Adres',@$Settings['adres']) }} {{ Form::bsText('tel','Telefon',@$Settings['tel']) }} {{ Form::bsText('whatsapp','Whatsapp',@$Settings['whatsapp'])
                                }} {{ Form::bsText('mail','İnfo Mail',@$Settings['mail']) }}
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="sosyal" aria-labelledby="profile-tab">
                                {{ Form::bsText('facebook','Facebook',@$Settings['facebook']) }} 
                                {{ Form::bsText('twitter','Twitter',@$Settings['twitter'])
                                }} {{ Form::bsText('instagram','Instagram',@$Settings['instagram']) }} 
                                {{ Form::bsText('linkedin','Linkedin',@$Settings['linkedin']) }}
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="google" aria-labelledby="profile-tab">
                                {{ Form::bsText('analytics','Analytics',@$Settings['analytics']) }} 
                                {{ Form::bsText('captcha','Recaptcha',@$Settings['captcha'])}}
                                {{ Form::bsText('facebookinstant','Facebook İnstant Code',@$Settings['facebookinstant'])}}                                
                                {{ Form::bsText('mapsapi','Maps Api',@$Settings['mapsapi']) }} 
                                {{ Form::bsText('maptitle','HaritaBaşlığı',@$Settings['maptitle']) }} 
                                {{ Form::bsText('lat','Koordinat lat',@$Settings['lat'])}} 
                                {{ Form::bsText('long','Koordinat long',@$Settings['long']) }}
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="modal" aria-labelledby="profile-tab">
                                {{ Form::bsText('modaltitle','Başlık',@$Settings['modaltitle']) }}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="asd">Zamanlama</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            </span>
                                            <input type="text" style="width: 100%" name="modaldate" id="daterangepicker" class="form-control" value="{{@$Settings['modaldate']}}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="asd">Zamanlama</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::select('item',array('1'=>'Birkere','2'=>'Sürekli'),@$Settings['modalshowtype'],array('class' => 'showtype full-width
                                        required','data-init-plugin'=>'select2',"data-disable-search"=>"true",'name'=>'modalshowtype'))
                                        !!}
                                    </div>
                                </div>
                                {{ Form::bsTextarea('modalcontent','İçerik',@$Settings['modalcontent'],['id'=>'summernote-editor'])}}

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="mailsettings" aria-labelledby="profile-tab">
                                {{ Form::bsText('smtp','Smtp',@$Settings['smtp']) }} 
                                {{ Form::bsText('user','User',@$Settings['user']) }}
                                {{ Form::bsText('fromname','Gönderen Adı',@$Settings['fromname']) }}  
                                {{ Form::bsText('tomail','Alıcı Mail',@$Settings['tomail']) }}
                                {{ Form::bsText('toname','Alıcı Adı',@$Settings['toname']) }}
                                {{ Form::bsText('password','Password',@$Settings['password'])}}
                                {{ Form::bsText('port','Port',@$Settings['port']) }}
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Kaydet</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
 
@section('js')
<script src="/cms/js/form.js"></script>
@endsection