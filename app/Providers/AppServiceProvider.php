<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //    \Carbon\Carbon::setLocale(config('app.locale'));
       
        
        setlocale(LC_TIME, config('app.locale'));
        date_default_timezone_set('CET');
        Carbon::setLocale(config('app.locale')); // app()->getDateFormat() ??
        Carbon::setToStringFormat('d/m/Y');
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
