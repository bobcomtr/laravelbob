<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Settings;
use App\Content;
class SettingsServiceProvider extends ServiceProvider
{
    public $html;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
   // protected $html="test";

   protected function menuBuilder($a,$child=false){
     
        foreach($a as $k=>$v){
            $rnd=rand(0,100000);
            $class="";
            $caret="<b class='caret'></b>";
            if(count($a[$k])>0){
            if (array_key_exists('children', $a[$k])){
                if($child){
                    $class="dropdown-submenu";
                    $caret="";
                }
                $link= $a[$k]['href']==''?'#':$a[$k]['href'];
                $this->html.= "<li class='".$class."'><a href='".$a[$k]['href']."' id='menu-".$rnd."' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>". $a[$k]['text'].$caret."</a>";
                $this->html.= "<ul class='dropdown-menu multi-level' aria-labelledby='menu-".$rnd."'>";
                $this->menuBuilder($a[$k]['children'],true);
                $this->html.= "</ul></li>";
               
            }else{
                $a[$k]['href']= $a[$k]['href']==''?'#':$a[$k]['href'];
                $this->html.=  "<li class='dropdown'><a href='".$a[$k]['href']."'>". $a[$k]['text']."</a></li>";
            }
         }
        }
    }

    public function boot(Factory $cache, Settings $settings)
    { 
     
        $settings = $cache->remember('settings', 60, function() use ($settings)
        {
            return $settings->pluck('value','keyword')->all();
        });
    
        config()->set('settings', $settings);
        
            //$cache->forget('menu');
         $menu = $cache->remember('menu', 60, function() use ($settings)
         {            
             $menus = Content::where('type', 'menu')->pluck('content')->toArray();
             $menus= json_decode($menus[0],true);
             //dd($menus);
             $this->html .=  "<ul class='nav navbar-nav'>";

             $this->html .=    $this->menuBuilder($menus);
           return  $this->html .=  "</ul>";

         });
        
         config()->set('menu', $menu);
      
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
