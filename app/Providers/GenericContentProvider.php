<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Content;
class GenericContentProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Factory $cache, Content $generic)
    { // $cache->forget('generic');
        
        $generic= $generic::where('status','published')->where('type','generic')->get();

        $generic = $cache->remember('generic', 60, function() use ($generic)
        {
            return $generic->pluck('content', 'slug')->all();
        });
  
        config()->set('generic', $generic);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
