<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;
class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', '_components.form.text', ['name','label', 'value'=> null, 'attributes'=> []]);
        Form::component('bsTextarea', '_components.form.textarea', ['name','label', 'value'=> null, 'attributes'=> []]);
        Form::component('bsPassword', '_components.form.password', ['name','label', 'value'=> null, 'attributes'=> []]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
