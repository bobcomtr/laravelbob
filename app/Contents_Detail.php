<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;
class Contents_Detail extends Model
{
    protected $table = 'contents_details';
    public $timestamps = false;
    protected $fillable = ['content_id', 'key', 'value'];


    public function content(){
        $this->belongsTo('App\Content');
    }
}
