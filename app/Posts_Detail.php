<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Post;

class Posts_Detail extends Eloquent
{
    protected $table = 'posts_details';
    public $timestamps = false;
    protected $fillable = ['post_id', 'key', 'value'];

   
    public function post(){
        $this->belongsTo('App\Post');
    }

}
