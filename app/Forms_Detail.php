<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Form;
class Forms_Detail extends Model
{
    protected $table = 'forms_details';
    public $timestamps = false;
    protected $fillable = ['form_id', 'key', 'value'];


    public function form(){
        $this->belongsTo('App\Form');
    }
}
