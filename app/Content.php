<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contents_Detail;
use App\User;
class Content extends Model
{
    use SoftDeletes;
    protected $table = 'contents';
    protected $fillable = ['title','slug','rank', 'published_at', 'user_id','excerpt','content','type','status'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function details(){
        return $this->hasMany('App\Contents_Detail');

    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    // public function children()
    // {
    //     return $this->hasMany('App\Post','parent_id');
    // }
}
