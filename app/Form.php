<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Forms_Detail;
class Form extends Model
{
    use SoftDeletes;
    protected $table = 'forms';
    protected $fillable = ['title','slug', 'published_at','type','status'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function details(){
        return $this->hasMany('App\Forms_Detail');

    }
}
