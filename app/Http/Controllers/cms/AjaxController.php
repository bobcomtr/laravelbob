<?php

namespace App\Http\Controllers\cms;
use App\lib\Slug;
use App\Category;
use App\Post;
use App\Content;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function post(Request $request){
        if ($request->process=="UrlSlug"){
            $slug = new Slug();
          
            $slug= $slug->createSlug($request->title,$request->lang,0,$request->type);
            return $slug;
        }
        if ($request->process=="getAltCategories"){
            $cat=Category::selectChild($request->id)->toArray();
            return $cat;
        }

        if ($request->process=="NewRowPosition"){
            $Model=$request->model;
            $posts = json_decode($request->ids);
            foreach ($posts as $p) {
                $post= $Model::findOrFail($p->id);
                $post->rank=$p->rank;
                $post->save();
            }
        }
        if($request->process=='getAltPages'){
            $page=Post::where('parent_id',$request->id)->with('children')->get();
            return $page;
        }

    }
}
