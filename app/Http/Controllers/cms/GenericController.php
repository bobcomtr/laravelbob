<?php

namespace App\Http\Controllers\cms;

use App\Posts_Detail;
use Illuminate\Http\Request;
use Carbon;
use Datatables;
use App\Content;
use Auth;
use App\Contents_Detail;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;
use App\lib\Slug;
class GenericController extends Controller
{
   
    public function index()
    {
        return view('cms.contents.generic_content.index');
    }

    public function edit($id)
    { 
        $generic = Content::where('type', 'generic')->find($id);

        $category=Contents_Detail::where('content_id',$id)->pluck('value','key');        

        return view('cms.contents.generic_content.edit')->with('generic', $generic)->with('details',$category);


    }

    public function update(Request $request, $id, Factory $cache)
    {
   
        $a=$this->validate($request, [
            'title' => 'required|max:255'
        ]);
        $post = Content::findOrFail($id);
        $post->slug = $request->slug;
        $post->title = $request->title;
        $post->excerpt = $request->excerpt;
        $post->content = $request->content;
        $post->status = $request->status;
        $post->published_at = Carbon\Carbon::now();
        $post->user_id = Auth::user()->id;

        if ($post->save()) {
            $cache->forget('generic');
            return response(['status' => 'success', 'title' => 'Ortak İçerik', 'message' => 'Başarıyla Güncellendi.']);
        } else {
            return response(['status' => 'error', 'title' => 'Ortak İçerik', 'message' => 'Bir hata oluştu']);
        }
    }
    public function create()
    {
        return view('cms.contents.generic_content.create');
    }
    public function store(Request $request, Factory $cache)
    {

        $this->validate($request, [
            'title' => 'required|max:255'
        ]);
       
        $content = new Content();
        $slug = new Slug();
        $content->slug = $slug->createSlug($request->title,$request->lang,'','generic');
        $last=Content::orderBy('created_at', 'desc')->take(1)->get();
        $last_id=$last[0]['id'];
      
        $request->request->add(['slug' => $content->slug,'rank'=>$last_id+1,'user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $content->fill($request->all());
        if ($content->save()) {
            $cache->forget('generic');
            $redirecturl=route('generic.edit', ['post' => $content->id]);
            return response(['status' => 'success', 'title' => 'Ortak İçerik', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
        } else {
            return response(['status' => 'error', 'title' => 'Ortak İçerik', 'message' => 'Bir Hata oluştu.']);
        }
    }
    public function destroy($id)
    {
        $generic = Content::where('type', 'generic')->find($id);
        if ($generic->delete()) {
            $redirecturl=route('generic.index');
            return response(['status' => 'success', 'title' => 'Ortak İçerik', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }
    public function genericDatatable()
    {
        $content = Content::select(['id', 'title', 'created_at', 'updated_at']);
        return Datatables::of(Content::query())
            ->addColumn('action', function ($content) {
                return '<a href="generic/' . $content->id . '/edit" class="btn btn-xs btn-primary pull-right"><i class="fa fa-edit"></i> Düzenle</a>';
            })
            ->where('type', 'generic')
            ->orderby('rank', 'asc')
            ->make(true);
    }
}