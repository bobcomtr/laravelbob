<?php

namespace App\Http\Controllers\cms;

use Illuminate\Http\Request;
use App\Settings;
use App\Content;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Storage;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;
use Carbon;
use Auth;

class AdminController extends Controller
{
    public function get_index(){
        return view('cms.index');
    }

     public function get_settings(){
      $Settings=Settings::all();
      $s="";
      foreach($Settings as $Setting){
           $s[$Setting->keyword]=$Setting->value;
      }    
     
      return view('cms.settings')->with("Settings",$s);
        }
  
    public function post_settings(Request $request, Factory $cache){
            $Setting="";
            $cache->forget('settings');
           $Validator= Validator::make($request->all(), [
                'logo' => 'mimes:jpg,jpeg,png,gif',
            ]);

            if(isset($request->logo)){
              if($Validator->fails()){
                 return response(['status'=>'error','title'=>'Ayarlar','message'=>'Resim Yüklemede Sorun Oluştu.']);
              }   
              //$logo=Input::file('logo');
              //$uzanti=Input::file('logo')->getClientOriginalExtension();
              //$logo_isim='logo.'.$uzanti;
             // Storage::disk('uploads')->makeDirectory('img');
              //Image::make($logo->getRealPath())->resize(200,100)->save('uploads/img/'.$logo_isim);
              //  $request->logo="asd";
               
            }
           try{
                Settings::truncate();         
                unset($request['_token']);
                foreach ($request->all() as $key => $value){      
                    if($value) $Setting=Settings::create(['keyword'=>"$key",'value'=>"$value"]);        
                }
                 return response(['status'=>'success','title'=>'Ayarlar','message'=>'Başarıyla Kaydedildi.']);
           }
           catch(\Exception $e){               
                return response(['status'=>'error','title'=>'Ayarlar','message'=>'Bir Hata oluştu.']);
           }         
             
            
    }

    public function getmenu(){
        $menu = Content::where('type', 'menu')->pluck('content')->toArray();
     
        return view('cms.contents.menu.index')->with('menu',$menu[0]);
    }
    public function postmenu(Request $request, Factory $cache){
        $menu = Content::where('type', 'menu')->delete();
       
        $user=Auth::user()->id;
        $page=Content::create(['title'=>'HeaderMenu',
        'type'=>'menu',
        'content' => $request->json,
        'slug'=>'headermenu',
        'published_at' => Carbon\Carbon::now(),
        'created_at'=>Carbon\Carbon::now(),
        'user_id'=>$user,        
        'status'=>'published',
        'rank'=>0,
        'excerpt'=>'-',]);
       if($page){           
        $cache->forget('menu');
        return response(['status' => 'success', 'title' => 'Menü', 'message' => 'Başarıyla Kaydedildi.']);

       }
    }
}
