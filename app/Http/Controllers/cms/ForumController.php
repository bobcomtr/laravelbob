<?php

namespace App\Http\Controllers\cms;

use App\Posts_Detail;
use Illuminate\Http\Request;
use Carbon;
use Datatables;
use App\Content;
use Auth;
use App\Contents_Detail;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;
use App\lib\Slug;
class ForumController extends Controller
{
   
    public function index()
    {
        return view('cms.contents.forum.index');
    }

    public function edit($id)
    { 
        $forum = Content::where('type', 'forum')->find($id);
        if($forum){
            $category=Contents_Detail::where('content_id',$id)->pluck('value','key'); 
            return view('cms.contents.forum.edit')->with('forum', $forum)->with('details',$category);
        }else{
            return redirect()->route('forum.index');
        }
       

    }

    public function update(Request $request, $id)
    {   
        $a=$this->validate($request, [
            'title' => 'required|max:255'
        ]);
        $post = Content::findOrFail($id);
        $post->slug = $request->slug;
        $post->title = $request->title;
        $post->excerpt = $request->excerpt;
        $post->content = $request->content;
        $post->status = $request->status;
        $post->published_at = Carbon\Carbon::now();
        $post->user_id = Auth::user()->id;

        if ($post->save()) {
            Contents_Detail::where('content_id', $post->id)->delete();

            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $banner_detail = Contents_Detail::create(['content_id' => $post->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            return response(['status' => 'success', 'title' => 'Forum içeriği', 'message' => 'Başarıyla Güncellendi.']);
        } else {
            return response(['status' => 'error', 'title' => 'Forum içeriği', 'message' => 'Bir hata oluştu']);
        }
    }
    public function create()
    {
        return view('cms.contents.forum.create');
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:255'
        ]);
       
        $content = new Content();
        $slug = new Slug();
        $content->slug = $slug->createSlug($request->title,$request->lang,'','forum');
        $last=Content::orderBy('created_at', 'desc')->take(1)->get();
        $last_id=$last[0]['id'];
      
        $request->request->add(['slug' => $content->slug,'rank'=>$last_id+1,'user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $content->fill($request->all());
        if ($content->save()) {
            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $content_detail = Contents_Detail::create(['content_id' => $content->id, 'key' => "$key", 'value' => "$value"]);
                }
            }            
            $redirecturl=route('forum.edit', ['post' => $content->id]);
            return response(['status' => 'success', 'title' => 'Forum içeriği', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
        } else {
            return response(['status' => 'error', 'title' => 'Forum içeriği', 'message' => 'Bir Hata oluştu.']);
        }
    }
    public function destroy($id)
    {
        $forum = Content::where('type', 'forum')->find($id);
        if ($forum->delete()) {
            $redirecturl=route('forum.index');
            return response(['status' => 'success', 'title' => 'Forum içeriği', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }
    public function forumDatatable()
    {
        $content = Content::select(['id', 'title','status', 'created_at', 'updated_at']);

        return Datatables::of(Content::query())
            ->addColumn('action', function ($content) {
                return '<a href="forum/' . $content->id . '/edit" class="btn btn-xs btn-primary pull-right"><i class="fa fa-edit"></i> Düzenle</a>';
            })
            ->editColumn('status', '{{__("cms.$status")}}')
            ->where('type', 'forum')
            ->orderby('rank', 'desc')
            ->make(true);
    }
}