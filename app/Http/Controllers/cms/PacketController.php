<?php

namespace App\Http\Controllers\cms;

use App\Posts_Detail;
use Illuminate\Http\Request;
use Carbon;
use Datatables;
use App\Content;
use Auth;
use App\Contents_Detail;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;

class PacketController extends Controller
{
   
    public function index()
    {
        return view('cms.contents.packets.index');
    }

    public function edit($id)
    { 
        $banner = Content::where('type', 'insurance-packet')->find($id);
        $category=Contents_Detail::where('content_id',$id)->pluck('value','key');  
        return view('cms.contents.packets.edit')->with('packet', $banner)->with('details',$category);

    }

    public function update(Request $request, $id, Factory $cache)
    {
       
        $a=$this->validate($request, [
            'title' => 'required|max:255',
            'detail.image' => 'required',
        ]);
        $brand = Content::findOrFail($id);
        $brand->slug = '';
        $brand->title = $request->title;
        $brand->excerpt = $request->excerpt;
        $brand->content = $request->content;
        $brand->status = $request->status;
        $brand->published_at = Carbon\Carbon::now();
        $brand->user_id = Auth::user()->id;

        if ($brand->save()) {
            Contents_Detail::where('content_id', $brand->id)->delete();

            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $brand_detail = Contents_Detail::create(['content_id' => $brand->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $cache->forget('packet');
            return response(['status' => 'success', 'title' => 'Sigorta Paketi', 'message' => 'Başarıyla Güncellendi.']);
        } else {
            return response(['status' => 'error', 'title' => 'Sigorta Paketi', 'message' => 'Bir hata oluştu']);
        }
    }
    public function create()
    {
        return view('cms.contents.packets.create');
    }
    public function store(Request $request, Factory $cache)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'detail.image' => 'required',
        ]);

        $content = new Content();
        $last=Content::orderBy('created_at', 'desc')->take(1)->get();
        $last_id=$last[0]['id'];
      
        $request->request->add(['rank'=>$last_id+1, 'slug'=>'','user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $content->fill($request->all());
        if ($content->save()) {
            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $content_detail = Contents_Detail::create(['content_id' => $content->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $redirecturl=route('packets.edit', ['post' => $content->id]);
            $cache->forget('packet');
            return response(['status' => 'success', 'title' => 'Sigorta Paketi', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
        } else {
            return response(['status' => 'error', 'title' => 'Sigorta Paketi', 'message' => 'Bir Hata oluştu.']);
        }
    }
    public function destroy($id)
    {
        $forum = Content::where('type', 'insurance-packet')->find($id);
        if ($forum->delete()) {
            $redirecturl=route('packets.index');
            $cache->forget('packet');
            return response(['status' => 'success', 'title' => 'Sigorta Paketi', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }
    public function packetDatatable()
    {
        $content = Content::select(['id', 'title', 'created_at', 'updated_at']);
        return Datatables::of(Content::query())
            ->addColumn('action', function ($content) {
                return '<a href="packets/' . $content->id . '/edit" class="btn btn-xs btn-primary pull-right"><i class="fa fa-edit"></i> Düzenle</a>';
            })
            ->editColumn('status', '{{__("cms.$status")}}')
            ->where('type', 'insurance-packet')            
            ->orderby('status', 'desc')
            ->orderby('rank', 'desc')
            ->make(true);
    }
}