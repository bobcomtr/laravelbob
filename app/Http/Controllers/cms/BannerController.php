<?php

namespace App\Http\Controllers\cms;

use App\Posts_Detail;
use Illuminate\Http\Request;
use Carbon;
use Datatables;
use App\Content;
use Auth;
use App\Contents_Detail;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;

class BannerController extends Controller
{
   
    public function index()
    {
        return view('cms.contents.banners.index');
    }

    public function edit($id)
    { 
        $banner = Content::where('type', 'banner')->find($id);
        $category=Contents_Detail::where('content_id',$id)->pluck('value','key');        

        return view('cms.contents.banners.edit')->with('banner', $banner)->with('details',$category);
    }

    public function update(Request $request, $id, Factory $cache)
    {
       
        $a=$this->validate($request, [
            'title' => 'required|max:255',
            'detail.image' => 'required',
        ]);
        $banner = Content::findOrFail($id);
        $banner->slug = '';
        $banner->title = $request->title;
        $banner->excerpt = $request->excerpt;
        $banner->content = $request->content;
        $banner->status = $request->status;
        $banner->published_at = Carbon\Carbon::now();
        $banner->user_id = Auth::user()->id;

        if ($banner->save()) {
            Contents_Detail::where('content_id', $banner->id)->delete();

            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $banner_detail = Contents_Detail::create(['content_id' => $banner->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $cache->forget('banner');
            return response(['status' => 'success', 'title' => 'Banner', 'message' => 'Başarıyla Güncellendi.']);
        } else {
            return response(['status' => 'error', 'title' => 'Banner', 'message' => 'Bir hata oluştu']);
        }
    }
    public function create()
    {
        return view('cms.contents.banners.create');
    }
    public function store(Request $request, Factory $cache)
    {

        $this->validate($request, [
            'title' => 'required|max:255',
            'detail.image' => 'required',
        ]);

        $content = new Content();
        $last=Content::orderBy('created_at', 'desc')->take(1)->get();
        $last_id=$last[0]['id'];
      
        $request->request->add(['rank'=>$last_id+1, 'slug'=>'','user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $content->fill($request->all());
        if ($content->save()) {
            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $content_detail = Contents_Detail::create(['content_id' => $content->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $redirecturl=route('banners.edit', ['post' => $content->id]);
            $cache->forget('banner');
            return response(['status' => 'success', 'title' => 'Banner', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
        } else {
            return response(['status' => 'error', 'title' => 'Banner', 'message' => 'Bir Hata oluştu.']);
        }
    }
    public function destroy($id)
    {
        $banner = Content::where('type', 'banner')->find($id);
        if ($banner->delete()) {
            $redirecturl=route('banners.index');
            return response(['status' => 'success', 'title' => 'Banner', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }

    public function bannerDatatable()
    {
        $content = Content::select(['id', 'title', 'created_at', 'updated_at']);

        return Datatables::of(Content::query())
            ->addColumn('action', function ($content) {
                return '<a href="banners/' . $content->id . '/edit" class="btn btn-xs btn-primary pull-right"><i class="fa fa-edit"></i> Düzenle</a>';
            })
            ->editColumn('status', '{{__("cms.$status")}}')
            ->where('type', 'banner')            
            ->orderby('status', 'desc')
            ->orderby('rank', 'desc')
            ->make(true);
    }
}