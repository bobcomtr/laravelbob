<?php

namespace App\Http\Controllers\cms;

use App\lib\Slug;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Controller;

class DescribingController extends Controller
{
    public function index(){

        $categories= Category::where('parent_id','0')->get();
        return view('cms.describing.index')->with('categories',$categories);
    }
    public function create()
    {
        $category=Category::where('parent_id','0')->pluck('title','id');        
        $category=collect(['' => 'Seçiniz'] + $category->all());
       
        return view('cms.describing.create')->with('categories',$category);
    }
    public function store(Request $request){
        $this->validate($request,[
            'title'=>'required|max:255',
            'slug'=>'required|max:255',
            'parent_id'=>'required'
        ]);

        $category=new Category();
        $slug=new Slug();
        $category->slug=$slug->createSlug($request->slug);
        $category->fill($request->all());
        if($category->save()){
            return response(['status'=>'success','title'=>'Kategori','message'=>'Başarıyla Kaydedildi.']);
        }else{
            return response(['status'=>'error','title'=>'Kategori','message'=>'Bir hata oluştu.']);
        }
    }
    public function edit($id){
        $category=Category::findOrFail($id);
        return view('cms.describing.edit')->with($category);
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'title'=>'required|max:255',
            'slug'=>'required|max:255',
            'parent_id'=>'required'
        ]);
        $category=new Category();
        $slug=new Slug();
        $category=Category::findOrFail($id);
        $category->slug=$slug->createSlug($request->slug,$request->lang,$id);
    }
    public function delete($id)
    {
        $category = Category::findOrFail($id);
        if ($category->delete()) {
            return redirect()->route('describing.index');
        }
    }
    public function categoryApi($id){
        
        $cat=Category::selectChild($id)->toArray();
        return response($cat, 200)
        ->header('Content-Type', 'application/json');
    }
}
