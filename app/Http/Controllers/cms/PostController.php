<?php

namespace App\Http\Controllers\cms;

use App\Post;
use App\User;
use App\Posts_Detail;
use App\lib\Slug;
use Illuminate\Http\Request;
use Auth;
use Carbon;
use Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $total = $request->input('total') ? $request->input('total') : "1";
        $posts = Post::OrderBy('id', 'desc')->paginate($total);
        return view('cms.posts.index')->with('posts', $posts);
    }

    public function create()
    {
        return view('cms.posts.create');
    }

    public function store(Request $request, Factory $cache)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:255'
        ]);

        $post = new Post();
        $slug = new Slug();
        $post->slug = $slug->createSlug($request->title);
        $last=Post::orderBy('created_at', 'desc')->take(1)->get();
        $last_id=$last[0]['id'];
        $request->request->add(['slug' => $post->slug,'rank'=>$last_id+1, 'user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $post->fill($request->all());

        if ($post->save()) {
            if(!$request->detail['image']==null){
            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $post_detail = Posts_Detail::create(['post_id' => $post->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $redirecturl=route('posts.edit', ['post' => $post->id]);
            $cache->forget('post');
            return response(['status' => 'success', 'title' => 'Yazı', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);        
        }else{
            return response(['status' => 'error', 'title' => 'Yazı', 'message' => 'Bir hata oluştu Görsel Yüklenmedi']);
         }
        } else {
            return response(['status' => 'error', 'title' => 'Yazı', 'message' => 'Bir Hata oluştu.']);
        }
    }

    public function edit($id)
    {
        $post = Post::find($id);
        $detail=Posts_Detail::where('post_id',$id)->pluck('value','key');       

        return view('cms.posts.edit')->with('post', $post)->with('details',$detail);
    }

    public function show($id)
    {
        $postWithUser = Post::with('user')->find($id);
        if ($postWithUser) {
            return view('cms.posts.show')->with('post', $postWithUser);
        } else {
            return redirect()->route('posts.index');
        }
    }

    public function update(Request $request, $id, Factory $cache)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:255'
        ]);

        $post = Post::findOrFail($id);
        $post->slug = $request->slug;
        $post->title = $request->title;
        $post->excerpt = $request->excerpt;
        $post->content = $request->content;
        $post->published_at = Carbon\Carbon::now();
        $post->user_id = Auth::user()->id;
        if ($post->save()) {
            if(!$request->detail['image']==null){
            Posts_Detail::where('post_id', $post->id)->delete();            
                foreach ($request->detail as $key => $value) {
                    if ($value) {
                        $post_detail = Posts_Detail::create(['post_id' => $post->id, 'key' => "$key", 'value' => "$value"]);
                   }
                }
                $redirecturl=route('posts.edit', ['post' => $post->id]);
                $cache->forget('post');
            return response(['status' => 'success', 'title' => 'Yazı', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
             }else{
                return response(['status' => 'error', 'title' => 'Yazı', 'message' => 'Görsel Yüklenmedi']);
             }
       } else {
            return response(['status' => 'error', 'title' => 'Yazı', 'message' => 'Bir hata oluştu']);
        }
    }

    public function destroy($id)
    {
        $post = Post::where('type', 'post')->find($id);
        if ($post->delete()) {
            $redirecturl=route('posts.index');
            $cache->forget('post');
            return response(['status' => 'success', 'title' => 'Yazı', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }

    public function datatable()
    {
        $posts = Post::select(['id', 'title', 'created_at','status', 'updated_at']);
       
        return Datatables::of(Post::query())
        ->addColumn('render', function ($posts) {
            $detail=Posts_Detail::where(['post_id'=>$posts->id,'key'=>'image'])->first();
            if($detail){
                $splitName = explode('/', $detail->value);
                $image=$splitName[0]."/".$splitName[1]."/".$splitName[2]."/thumbs/".$splitName[count($splitName)-1]; 
                return '<img src=" '.$image.' "  width="auto" height="50px"/>'; 
            }
           
        })
            ->addColumn('action', function ($posts) {
                return '<a href="posts/'.$posts->id.'/edit" class="btn btn-xs btn-primary pull-right">Düzenle</a>';
            })
            ->editColumn('status', '{{__("cms.$status")}}')
        ->where('type','post')
        ->orderby('rank', 'desc')
        ->rawColumns(['render', 'action'])      
        ->make(true);
    }

}














//POST DETAIL
/*
 $post = Post::find(30);
 $postdetail = $post->details()->get()->toArray();
 dd($postdetail);
 */

/*
$postWithDetail=Post::with('details')->find(30)->toArray();
dd($postWithDetail);
*/

//Users Post

/*
$user = User::find(3);
$userallpost = $user->posts()->get()->toArray();
dd($userallpost);
*/

/*
$userallpost = User::with('posts')->find(3)->toArray();
dd($postWithDetail);
*/


// $post = Post::find(28);
// $postuser =  $post->user()->get()->toArray();
//dd($postuser);
