<?php

namespace App\Http\Controllers\cms;

use App\Post;
use App\User;
use App\Posts_Detail;
use App\lib\Slug;
use Illuminate\Http\Request;
use Auth;
use Carbon;
use Datatables;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index(){
        $page=Post::where([['type','=','page'],
                           ['parent_id','=',0]])->orderBy('id','desc')->get();
        return view('cms.pages.index')->withPage($page);
    }
    public function create()
    {
        return view('cms.pages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:255',
            'parent_id'=>'required'
        ]);
      
        $post = new Post();
        $slug = new Slug();
        $post->slug = $slug->createSlug($request->title);
        $request->request->add(['slug' => $post->slug, 'user_id' => Auth::user()->id, 'published_at' => Carbon\Carbon::now()]);
        $post->fill($request->all());

        if ($post->save()) {
            foreach ($request->detail as $key => $value) {
                if ($value) {
                    $post_detail = Posts_Detail::create(['post_id' => $post->id, 'key' => "$key", 'value' => "$value"]);
                }
            }
            $redirecturl=route('pages.edit', ['post' => $post->id]);
            return response(['status' => 'success', 'title' => 'Sayfa', 'message' => 'Başarıyla Kaydedildi.', 'redirect' => $redirecturl]);
     
        } else {
            return response(['status' => 'error', 'title' => 'Sayfa', 'message' => 'Bir Hata oluştu.']);
        }
    }

    public function edit($id)
    {
        $post = Post::find($id);
        $detail=Posts_Detail::where('post_id',$id)->pluck('value','key');       

        return view('cms.pages.edit')->with('post', $post)->with('details',$detail);
    }

    public function show($id)
    {
        $postWithUser = Post::with('user')->find($id);
        if ($postWithUser) {
            return view('cms.pages.show')->with('post', $postWithUser);
        } else {
            return redirect()->route('pages.index');
        }
    }

    public function update(Request $request, $id)
    {
    
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:255'
        ]);

        $post = Post::findOrFail($id);
        $post->slug = $request->slug;
        $post->title = $request->title;
        $post->excerpt = $request->excerpt;
        $post->content = $request->content;
        $post->published_at = Carbon\Carbon::now();
        $post->user_id = Auth::user()->id;
        if ($post->save()) {
            Posts_Detail::where('post_id', $post->id)->delete();            
                foreach ($request->detail as $key => $value) {
                    if ($value) {
                        $post_detail = Posts_Detail::create(['post_id' => $post->id, 'key' => "$key", 'value' => "$value"]);
                   }
                }
                return response(['status' => 'success', 'title' => 'Sayfa', 'message' => 'Başarıyla Güncellendi.']);            
       } else {
            return response(['status' => 'error', 'title' => 'Sayfa', 'message' => 'Bir hata oluştu']);
        }
    }

    public function destroy($id)
    {
        $page = Post::where('type', 'page')->find($id);
        if ($page->delete()) {
            $redirecturl=route('pages.index');
            return response(['status' => 'success', 'title' => 'Sayfa', 'message' => 'Başarıyla Silindi.', 'redirect' => $redirecturl ]);
        }
    }

}
