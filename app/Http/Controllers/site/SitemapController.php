<?php

namespace App\Http\Controllers\site;

use App\Post;
use App\Posts_Detail;
use App\Content;
use App\Contents_Detail;
use Carbon;
use App\Describing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;

class SitemapController extends Controller
{
    public $html="";
    public function sitemap_generator($posts,$slug){
        foreach ($posts as $post){ 
            $this->html.= "<url>
                <loc>". url('/').$slug.$post->slug."</loc>
                <lastmod>".date('c',strtotime($post->published_at))."</lastmod>
                <changefreq>never</changefreq>
                <priority>0.60</priority>
            </url>";
            $alt= Post::where('parent_id',$post->id)->with('children')->get();
            if($alt){
                $newslug=$slug.$post->slug.'/';
                $this->sitemap_generator($alt,$newslug);
            }
        }
        return $this->html;
    }
    public function index(Factory $cache)
    {   
        //$cache->forget('sitemap');
        $sitemapcache = $cache->remember('sitemap', 60, function()
        {
            $sitemap="";
            $makaleler = Post::where(['status'=>'published','type'=>'page'])->get(); 
            $sitemap.= $this->sitemap_generator($makaleler,'/');
            $makaleler = Post::where(['status'=>'published','type'=>'post'])->get(); 
            $sitemap.= $this->sitemap_generator($makaleler,'/blog/');
            $makaleler = Content::where(['status'=>'published','type'=>'forum'])->get(); 
            $sitemap.= $this->sitemap_generator($makaleler,'/forum/');
            return $sitemap;
        });
    
        config()->set('sitemap', $sitemapcache);


      
        return response()->view('site.sitemap', [
            'sitemap' => $sitemapcache,
        ])->header('Content-Type', 'text/xml');
    }


}
