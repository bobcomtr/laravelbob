<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use App\Content;
use App\Http\Controllers\Controller;
use App\Contents_Detail;
use Illuminate\Support\Facades\DB;

class ForumController extends Controller
{
    public function index(){
        $post=Content::with('details')->where('type','forum')->where('status','published')->orderBy('rank',"desc")->paginate(10);
        return view('site.forums')->withPosts($post);
    }

    public function show(Request $request){
      
        $post=Content::where('slug',$request->slug)->where('status','published')->first(); 
        if ($post) {
            $detail=Contents_Detail::where('content_id',$post->id)->pluck('value','key');
            return view('site.forum')->withPost($post)->with('Details',$detail);
        } else {
            return view('site.404');
        }
       
    }
}
