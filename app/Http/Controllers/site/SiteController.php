<?php

namespace App\Http\Controllers\site;

use App\Post;
use App\Posts_Detail;
use App\Content;
use App\Contents_Detail;
use Carbon;
use App\Describing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;



class SiteController extends Controller
{
    
    public function slug(Request $request)
    {
        $path = explode('/', $request->path());

        $post = Post::where([['slug', $request->slug], ['lang', $path[0]]])->first();

        if ($post) {
            return view('site.posts')->withPost($post);
        } else {
            return view('site.404');
        }
    }

    public function getBanner(){
        //$banner = Content::where('type','banner')->where('status','published')->get();
        
        $banner = Content::with('details')->where('type','banner')->where('status','published')->get()->toArray();    
        return response($banner, 200)
        ->header('Content-Type', 'application/json');
    }

    public function index(Factory $cache)
    {
        $banner = $cache->remember('banner', 60, function()
        {
            return  Content::with('details')->where('type','banner')->where('status','published')->orderBy('rank','desc')->get();      
        });
        config()->set('banner', $banner);

        $brand = $cache->remember('brand', 60, function()
        {
            return Content::with('details')->where('type','brand')->where('status','published')->orderBy('rank','desc')->get();
        });
        config()->set('brand', $brand);

        $blog = $cache->remember('blog', 60, function()
        {
            return Post::with('details')->where('type','post')->where('status','published')->orderBy('rank','desc')->take(3)->get();
      
        });
        config()->set('blog', $blog);

        $pakets = $cache->remember('packet', 60, function()
        {
            return Content::with('details')->where('type','insurance-packet')->where('status','published')->orderBy('rank','desc')->get();
      
        });
        config()->set('packet', $pakets);

        return view('site.index')->with("banner",$banner)->with("brand",$brand)->with("blogs",$blog)->with('packets', $pakets);

    }

    public function getPage($slug = null)
    {
        $slugs = collect(func_get_args());
           
            $page = $slugs->reduce(function($page, $slug) {    
                return ($page->children()->where('slug', $slug)->first());
            }, Post::whereSlug($slugs->shift())->with('children')->first());
            
       
            if($page==null){
             return view('site.404');
            }else{
                $detail=Posts_Detail::where('post_id',$page->id)->pluck('value','key');
                return view('site.page')->withPost($page)->with('Details',$detail);
            }
    

        if(!$page){
         return view('site.404');

        }else{
            return view('site.posts')->with('post',$page);
        }
   
    }
    public function forum(){
        return view('site.forum');
    }
    public function contact(){
        return view('site.iletisim');
    }

    public function contactUS(){
        return view('site.contactUS');
    }

}
