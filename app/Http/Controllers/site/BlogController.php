<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Controllers\Controller;
use App\Posts_Detail;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(){
        $post=Post::with('details')->where('type','post')->where('status','published')->orderBy('rank','desc')->paginate(10);

        return view('site.posts')->withPosts($post);
    }

    public function show(Request $request){      
        $post=Post::where('slug',$request->slug)->first(); 
        if ($post) {
            $detail=Posts_Detail::where('post_id',$post->id)->pluck('value','key');
            return view('site.post')->withPost($post)->with('Details',$detail)->with('url',$request->slug);
        } else {
            return view('site.404');
        }
       
    }
    public function amp(Request $request){ 
        $post=Post::where('slug',$request->slug)->first(); 
        if ($post) {
            $detail=Posts_Detail::where('post_id',$post->id)->pluck('value','key');
            return view('site.amp')->withPost($post)->with('Details',$detail)->with('url',$request->slug);
        } else {
            return view('site.404');
        }     
    }
}
