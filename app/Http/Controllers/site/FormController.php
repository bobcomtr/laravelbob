<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mail;
use Carbon;
use App\Mail\contactus;
//use PHPMailer;
//use Mailer\Exception;
//use PHPMailer;
//use ler\Exception;

//Load composer's autoloader
//require '../BobCMS/vendor/autoload.php';
//require '../vendor/phpmailer/phpmailer/src/Exception.php';
require_once base_path() . '/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once base_path() . '/vendor/phpmailer/phpmailer/src/SMTP.php';
require_once base_path() . '/vendor/phpmailer/phpmailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

use App\Form;
use App\Forms_Detail;
class FormController extends Controller
{
    public function index($slug = null){
        if($slug=="hayat-sigortasi"){$title="Hayat Sigortası";}
        elseif($slug=="isyeri-sigortasi"){$title="İşyeri Sigortası";}
        elseif($slug=="saglik-sigortasi"){$title="Sağlık Sigortası";}
        elseif($slug=="yabanci-saglik-sigortasi"){$title="Yabancı Sağlık Sigortası";}
        else{ $title= ucwords(str_replace('-', ' ', $slug)); }
       
        return view('site.form')->with('form',$slug)->with('title',$title);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function send(Request $request){
        $formname= $request->formname;
        unset($request['_token']);
        unset($request['honeypot']);
        $post = $request->all();
        $mail = new PHPMailer(true);
        try{
            $mail->isSMTP();
            $mail->CharSet ='utf-8';
            $mail->SMTPAuth ='true' ; #set it true
            $mail->SMTPSecure = '';
            $mail->Host = config('settings.smtp'); #gmail has host  smtp.gmail.com
            $mail->Port =config('settings.port'); #gmail has port  587 . without double quotes
            $mail->Username =config('settings.user'); #your username. actually your email
            $mail->Password =config('settings.password'); # your password. your mail password
            $mail->setFrom(config('settings.user'), config('settings.fromname')); 
            $mail->Subject = $formname;           
           
            $mail->MsgHTML(view('email.contactus')->with('Post',$post));
            $mail->addAddress(config('settings.tomail') ,config('settings.toname')); 
            $result =  $mail->send();





            $form = new Form();          
            $request->request->add(['type'=>$formname,'status'=>'send','title'=>$formname,'slug'=>'-', 'published_at' => Carbon\Carbon::now()]);
            $form->fill($request->all());
            if ($form->save()) { 
                unset($request['type']);
                unset($request['status']);
                unset($request['title']);
                unset($request['slug']);
                unset($request['published_at']);

                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $form_detail = Forms_Detail::create(['form_id' => $form->id, 'key' => "$key", 'value' => "$value"]);
                    }
                }
            }





            return response(['status' => 'success', 'title' => '', 'message' => 'Formdaki bilgileriniz Başarıyla Gönderildi.']);

            //return back()->with('success', 'Mesajınız Başarıyla Gönderildi.'); 
        }catch(phpmailerException $e){
            dd($e);
        }catch(Exception $e){
            dd($e);
        } 
    }
 
    

}
