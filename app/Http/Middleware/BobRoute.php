<?php

namespace App\Http\Middleware;

use App\Post;
use Closure;

class BobRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    
    public function handle($request, Closure $next)
    {

        $page = Post::where('slug', $request->path())->where('status', 1);

        $page = $page->first();
        if ($page) {
            return view('site.posts')->with('post', $page);
        }else{

            return false;
        }



    }
}
