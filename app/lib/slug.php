<?php

namespace App\lib;
use App\Post;
use App\Content;
class Slug
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function createSlug($title,$lang='tr', $id = 0,$type='post')
    {
        $title = str_replace(
            ['ü', 'Ü', 'ö', 'Ö'],
            ['u', 'U', 'o', 'O'],
            $title
        );
        $slug = str_slug($title);       
        $allSlugs = $this->getRelatedSlugs($slug,$lang, $id,$type);
       
             if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        
        throw new \Exception('hata');
    }
    protected function getRelatedSlugs($slug,$lang='tr', $id = 0,$type='post')
    {
       
        if($type!='post' && $type!='page'){
            return Content::select('slug')->where('slug', 'like', $slug.'%')
            ->where([['id', '<>', $id],['lang','=',$lang],['type','=',$type]])
            ->get();
        }else{
            return Post::select('slug')->where('slug', 'like', $slug.'%')
            ->where([['id', '<>', $id],['lang','=',$lang]])
            ->get();
        }
        
       
    }
}
?>