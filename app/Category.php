<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $fillable = ['title', 'key', 'slug','description','parent_id'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public static function selectChild($id)
    {
        $categories=Category::where('parent_id',$id)->get(); //rooney

      //  $categories=$this->addRelation($categories);

        return $categories;

    }
public static function getName($id){
    $categories=Category::where('id',$id)->get();
    return $categories->title;
}

}
