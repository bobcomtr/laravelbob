<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contactus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('email.contactus')
        //             ->with([
        //                 'name' => 'adım',
        //                 'email' => 'mailim',
        //                 'user_message'=>'mesajımız'
        //             ]);

                    // $this->withSwiftMessage(function ($message) {
                    //     $message->getHeaders()
                    //             ->addTextHeader('Custom-Header', 'HeaderValue');
                    // });

                    $address = 'onur@bob.com.tr';
 
      $name = 'Saquib Rizwan';
 
      $subject = 'Laravel Email';
 
      return $this->view('email.contactus')
 
      ->from($address, $name)
 
      ->subject($subject);
    }
}
