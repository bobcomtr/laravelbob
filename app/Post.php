<?php

namespace App;

use Illuminate\Database\Eloquent\Model  as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Posts_Detail;
use App\User;
class Post extends Eloquent
{
    use SoftDeletes;
    protected $table = 'posts';
    protected $fillable = ['title', 'published_at', 'user_id','parent_id','excerpt','content','slug','rank','type','status'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true;
    
    public function details(){
        return $this->hasMany('App\Posts_Detail');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function children()
    {
        return $this->hasMany('App\Post','parent_id');
    }

}
